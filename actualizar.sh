#!/bin/bash

DIRECTORIO='/home/david/configs-david'

ROJO='\e[1;31m'
AZUL='\e[1;34m'
NORMAL='\e[0m'
LINEA="${ROJO}**************************************************************${NORMAL}"

echo -e "${LINEA}\n${AZUL}Actualizando los scripts${NORMAL}\n${LINEA}"

cd $DIRECTORIO
git pull

echo -e "${LINEA}\n${AZUL}Actualizando archivos de latex${NORMAL}\n${LINEA}"

# solo si son diferentes...
cmp -s ~/configs-david/latex/ism-commands.sty ~/texmf/tex/latex/ism-commands.sty > /dev/null

if [ $? -eq 1 ]; then
    cp ~/configs-david/latex/ism-commands.sty ~/texmf/tex/latex/
    texhash
fi
