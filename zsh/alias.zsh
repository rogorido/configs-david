#aliases

#alias pacman='pacman-color'

alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias  vi=$(which vim)
alias -- -='cd -'
alias  ...='../..'
alias  ....='../../..'
alias  .....='../../../..'
alias  ll='ls -lh'
alias  sl='ls -lah'
alias  l='ls -la'

# copias de seguridad
alias oviedo='~/bin/copiaseguridad.sh -o'
alias casa='~/bin/copiaseguridad.sh -c'
alias pincel='~/bin/copiaseguridad.sh -p'
alias discoduro='~/bin/copiaseguridad.sh -d'
alias actualizar='~/bin/actualizar.sh'
alias sistema='sudo apt-get update && sudo apt-get upgrade'

# otros alias mas..
alias rm='rm -i'
alias rmt='trash-put'
alias grep='grep --color'
alias rgrep='rgrep --color'
alias lo='ls -alSorh'
alias dm='du --max-depth=1 -h'
#alias dmo='du -kxh | egrep -v "\./.+/" | sort -n'
alias lsd='ls -d *(/)' # listar los directorios
# alias de git
alias gp='git pull'

alias lad='ls -d -- .*(/)'				# only show dot-directories
alias lsa='ls -a -- .*(.)'				# only show dot-files
alias lsd='ls -d -- *(/)'				# only show directories
alias lse='ls -ld -- *(/^F)'				# only show empty directories
alias lsen='ls -d *(/om[1])'				# newest directory
alias lsh='ls -l *(m-1)'				# modificados hoy
alias lsp='ls -lha *.pdf'				# pdfs
alias lsdo='ls -lha *.{doc,DOC,docx}'				# words

# FIXME: Replace the ``grep -v'', ``2&>/dev/null'' by zsh-only syntax
alias lsnew='=ls -rtlh -- *(.) | tail $@ 2&>/dev/null'  # only the newest files
alias lsold="=ls -rtl -- *(.) | head $@ 2&>/dev/null"   # display the oldest files
alias lssmall="=ls -Sl -- *(.) | tail $@ 2&>/dev/null"  # display the smallest files
alias lsx='ls -l -- *(*) | head $@ 2&>/dev/null'        # only show executables
alias lssuid='ls -l -- *(s,S) | head $@ 2&>/dev/null'   # only show suid-files
alias lsbig="ls -lSh -- *(.) | grep -v total | head $@ 2&>/dev/null"    # display the biggest files
# show sorted directory sizes for all directories
alias dua='du -s *(/DN) | sort -nr | cut -f 2- | while read a; do du -sh "$a"; done'
# show sorted directory sizes for visible directories only
alias duv='du -s *(/N) | sort -nr | cut -f 2- | while read a; do du -sh "$a"; done'
# show sorted directory sizes for hidden directories only
alias duh='du -s .*(/N) | sort -nr | cut -f 2- | while read a; do du -sh "$a"; done'

#alias -s {jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF}='feh'
alias -s {jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF}='qiv'
alias -s {mp4,flv,avi,mp3,ogg,wav,wmv,m4a,M4A}='mplayer'
alias -s {doc,odt,xls,docx,xlsx,ods}='soffice'
alias -s {tex,txt}='vim'
alias -s djvu=djview4
alias -s chm=xchm

alias -g G='|grep'

### Local Variables: ***
### mode: sh ***
### End: ***
