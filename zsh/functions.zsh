# entiendo que lo de (N) es para que no haya mensaje de error
# pues es la NULL_GLOB option
clean()
{
    rm -f *.aux(N)
    rm -f *.log(N)
    rm -f *.out(N)
    rm -f *.bbl(N)
    rm -f *.blg(N)
    rm -f *.toc(N)
    rm -f *~(N)
    rm -f .*~(N)
    rm -f *.run.xml(N)
    rm -f *-blx.bib(N)
    rm -f \#*\#(N)
    rm -f .dvi(N)
}

#f4# Use German Wikipedia's full text search
swiki()   {
    emulate -L zsh
    ${=BROWSER} http://de.wikipedia.org/wiki/Spezial:Search/${(C)1}
}

#f4# Search German   Wikipedia
wd()  {
    emulate -L zsh
    ${=BROWSER} http://de.wikipedia.org/wiki/"${(C)*}"
}

# list images only
limg() {
    local -a images
    images=( *.{jpg,gif,png,jpeg,jpe,JPG}(.N) )

    if [[ $#images -eq 0 ]] ; then
        print "No image files found"
    else
        ls "$images[@]"
    fi
}

#f5# Show some status info
status() {
    print
    print "Date..: "$(date "+%Y-%m-%d %H:%M:%S")
    print "Shell.: Zsh $ZSH_VERSION (PID = $$, $SHLVL nests)"
    print "Term..: $TTY ($TERM), ${BAUD:+$BAUD bauds, }$COLUMNS x $LINES chars"
    print "Login.: $LOGNAME (UID = $EUID) on $HOST"
    print "System: $(cat /etc/[A-Za-z]*[_-][rv]e[lr]*)"
    print "Uptime:$(uptime)"
    print
}

weather() {
    emulate -L zsh
    [[ -n "$1" ]] || {
        print 'Usage: weather <station_id>' >&2
        print 'List of stations: http://en.wikipedia.org/wiki/List_of_airports_by_ICAO_code'>&2
        return 1
    }

    local VERBOSE="yes"    # TODO: Make this a command line switch

    local ODIR=`pwd`
    local PLACE="${1:u}"
    local DIR="${HOME}/.weather"
    local LOG="${DIR}/log"

    [[ -d ${DIR} ]] || {
        print -n "Creating ${DIR}: "
        mkdir ${DIR}
        print 'done'
    }

    print "Retrieving information for ${PLACE}:"
    print
    cd ${DIR} && wget -T 10 --no-verbose --output-file=$LOG --timestamping http://weather.noaa.gov/pub/data/observations/metar/decoded/$PLACE.TXT

    if [[ $? -eq 0 ]] ; then
        if [[ -n "$VERBOSE" ]] ; then
            cat ${PLACE}.TXT
        else
            DATE=$(grep 'UTC' ${PLACE}.TXT | sed 's#.* /##')
            TEMPERATURE=$(awk '/Temperature/ { print $4" degree Celcius / " $2" degree Fahrenheit" }' ${PLACE}.TXT | tr -d '(')
            echo "date: $DATE"
            echo "temp:  $TEMPERATURE"
        fi
    else
        print "There was an error retrieving the weather information for $PLACE" >&2
        cat $LOG
        cd $ODIR
        return 1
    fi
    cd $ODIR
}

# Get a 7 chars password: generate-password 7 
generate-password() {
    strings /dev/urandom | grep -o '[[:alnum:]]' | head -n $1 | tr -d '\n'; echo
}

# use google for a definition 
define() {
  local lang charset tmp

  lang="${LANG%%_*}"
  charset="${LANG##*.}"
  tmp='/tmp/define'
  
  lynx -accept_all_cookies \
       -dump \
       -hiddenlinks=ignore \
       -nonumbers \
       -assume_charset="$charset" \
       -display_charset="$charset" \
       "http://www.google.com/search?hl=$lang&q=define%3A+$1&btnG=Google+Search" | grep -m 5 -C 2 -A 5 -w "*" > "$tmp"

  if [[ ! -s "$tmp" ]]; then
    echo -e "No definition found.\n"
  else
    echo -e "$(grep -v Search "$tmp" | sed "s/$1/\\\e[1;32m&\\\e[0m/g")\n"
  fi

  rm -f "$tmp"
}

#f5# Make screenshot
sshot() {
    [[ ! -d ~/shots  ]] && mkdir ~/shots
    #cd ~/shots ; sleep 5 ; import -window root -depth 8 -quality 80 `date "+%Y-%m-%d--%H:%M:%S"`.png
    cd ~/shots ; sleep 5; import -window root shot_`date --iso-8601=m`.jpg
}

#f5# List files which have been changed within the last {\it n} days, {\it n} defaults to 1
changed() {
    emulate -L zsh
    print -l -- *(c-${1:-1})
}

# Exchange ' ' for '_' in filenames.
unspaceit()
{
        for _spaced in "${@:+"$@"}"; do
                if [ ! -f "${_spaced}" ]; then
                        continue;
                fi
                _underscored=$(echo ${_spaced} | tr ' ' '_');
                if [ "${_underscored}" != "${_spaced}" ]; then
                        mv "${_spaced}" "${_underscored}";
                fi
        done
}


# convert the Flash Apps to AVI and/or MPEG files.
# MISC: convert the Flash Apps to AVI and/or MPEG files.
vlf2mpeg() {
    if [[ -z "$1" || ! -e "$1" ]]; then
      echo Usage: $0 VideoFile.vlf
      echo Use http://keepvid.com to download the VLF file.
    else
      ffmpeg -i $1 -ab 56 -ar 22050 -b 500 -s 320x240 ${1:r}.mpeg
    fi
}

# MISC: Globbing is simple? Sure .. See zshexpn(1) /Glob Qualifiers for details and come back ;) 
function ayuda()
{
echo -e "
      actualizar               actualizar scripts y plantillas de latex
      casa                     pasar a Oviedo (dentro de casa)
      discoduro                pasar al discoduro
      extraer-audio [fichero]  extraer audio
      limpiardisco             borra archivos innecesarios del sistema
      oviedo                   pasar a Oviedo
      pincel                   pasar al pincel
      pasar-a-epub             pasar fichero orgmode a epub
      sistema                  actualizar el sistema"

     
}

function extraer-audio() {
    if [[  ! -e "$1" ]]; then
      echo Paspancio: el fichero no existe
    else
	FICHERO="$1"
	FICHEROBASE=${FICHERO%.*}

	avconv -i "${FICHERO}" -vn -f mp3 "${FICHEROBASE}.mp3"
    fi
    
}

function pasar-a-epub() {
    if [[  ! -e "$1" ]]; then
      echo Paspancio: el fichero no existe
    else
	FICHERO="$1"
	FICHEROBASE=${FICHERO%.*}

	pandoc ${FICHERO} -o ${FICHEROBASE} --toc
    fi
    
}


function limpiardisco() {
    sudo apt-get clean

}

### Local Variables: ***
### mode: sh ***
### End: ***
