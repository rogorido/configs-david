#!/bin/bash

if [[ $# -lt 1 ]]
then
    echo "Es necesaria al menos una opción:"
    echo "   -o para enviar a Oviedo"
    echo "   -c para enviar dentro de la propia casa"
    echo "   -p para enviar al pincel"
    exit 1
fi

while getopts "ocpd" option
do
    case "${option}"
        in
        
        o) DESTINO='david@oviedocomputadora.chickenkiller.com:/home/david/copiaseguridad';;
        c) DESTINO='david@192.168.1.101:/home/david/copiaseguridad';;
        p) DESTINO='/media/david/TOSHIBA/copiaseguridad';;
        d) DESTINO='/media/david/HP Portable Drive/copiaseguridad';;

    esac
done

ROJO='\e[1;31m'
AZUL='\e[1;34m'
NORMAL='\e[0m'
LINEA="${ROJO}**************************************************************${NORMAL}"

echo -e "${LINEA}\n${AZUL}Pasando general${NORMAL}\n${LINEA}"
rsync -avz /home/david/Escritorio/general "${DESTINO}" --progress

echo -e "${LINEA}\n${AZUL}Pasando zotero${NORMAL}\n${LINEA}"
rsync -avz /home/david/.mozilla/firefox/mwad0hks.default/zotero "${DESTINO}" --progress

echo -e "${LINEA}\n${AZUL}Pasando firefox${NORMAL}\n${LINEA}"
rsync -avz /home/david/.mozilla/firefox/mwad0hks.default/*.sqlite "${DESTINO}" --progress

echo -e "${LINEA}\n${AZUL}Pasando mnemosyne${NORMAL}\n${LINEA}"
rsync -avz /home/david/.local/share/mnemosyne "${DESTINO}" --progress
