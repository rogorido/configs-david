(message "Cargando configuraciones de Calendario...")

(setq calendar-week-start-day 1
          calendar-day-name-array ["Domingo" "Lunes" "Martes" "Miércoles"
                                   "Jueves" "Viernes" "Sábado"]
          calendar-month-name-array ["Enero" "Febrero" "Marzo" "Abril" "Mayo"
                                     "Junio" "Julio" "Agosto" "Septiembre"
                                     "Octubre" "Noviembre" "Diciembre"])

(add-hook 'calendar-load-hook
              (lambda ()
                (calendar-set-date-style 'european)))

(setq
  calendar-mark-holidays-flag        t
  all-christian-calendar-holidays   t        ;; show christian
;  all-islamic-calendar-holidays     nil      ;; don't show islamic
;  all-hebrew-calendar-holidays      nil      ;; don't show hebrew
  display-time-24hr-format          t        ;; use 24h format
  display-time-day-and-date         nil      ;; don't display time
  calendar-latitude                 43.21   ;; Oviedo 
  calendar-longitude                5.50    ;; Oviedo
  calendar-location-name "Oviedo"
)
; ver abajo porque tengo unas funciones para lo del sunrise/sunset

(setq   holiday-general-holidays nil   ; get rid of too U.S.-centric holidays
        holiday-hebrew-holidays nil    ; get rid of religious holidays
        holiday-islamic-holidays nil   ; get rid of religious holidays
        holiday-oriental-holidays nil  ; get rid of Oriental holidays
        holiday-bahai-holidays nil)     ; get rid of Baha'i holidays

; añado esto de:
; http://www.et.bs.ehu.es/~etpmohej/.emacs_win

(setq calendar-time-display-form
      '(24-hours ":" minutes (and time-zone (concat " (" time-zone ")"))))
(setq calendar-date-display-form
      '((if dayname (concat dayname ", ")) day " " monthname " " year))

(setq solar-n-hemi-seasons
      '("Primavera" "Verano" "Otoño" "Invierno"))

(setq holiday-general-holidays
      '((holiday-fixed 1 1 "Año Nuevo")
        (holiday-fixed 5 1 "Primero de Mayo")
        (holiday-fixed 10 12 "Día Nacional")))

;; Feiertage für Bayern, weitere auskommentiert
(setq holiday-christian-holidays
      '((holiday-fixed 12 25 "1. Navidad")
        (holiday-fixed 1 6 "Tres Reyes Mayos")
        (holiday-easter-etc -48 "Rosenmontag")
        (holiday-easter-etc -3 "Jueves santo")
        (holiday-easter-etc  -2 "Viernes Santo")
        (holiday-easter-etc   0 "Domingo de Pascua")
        (holiday-fixed 11 1 "Todos los santos")))

; con este código se consigue que salgan
; los números de la semana con M-x calendar
(copy-face font-lock-constant-face 'calendar-iso-week-face)
(set-face-attribute 'calendar-iso-week-face nil
                    :height 0.7)
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'calendar-iso-week-face))

(provide 'ism-calendar)
