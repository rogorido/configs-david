


(org-add-link-type "ebib" 'ebib)

(org-add-link-type 
 "cite" 'ebib
 (lambda (path desc format)
   (cond
    ((eq format 'latex)
     (if (or (not desc) (equal 0 (search "cite:" desc)))
	 (format "\\cite{%s}" path)
       (format "\\cite[%s]{%s}" desc path)
       )))))
  
(org-add-link-type 
 "parencite" 'ebib
 (lambda (path desc format)
   (cond
    ((eq format 'latex)
     (if (or (not desc) (equal 0 (search "parencite:" desc)))
	 (format "\\parencite{%s}" path)
       (format "\\parencite[%s]{%s}" desc path)
       )))))
  
  (org-add-link-type 
     "textcite" 'ebib
     (lambda (path desc format)
       (cond
        ((eq format 'latex)
         (if (or (not desc) (equal 0 (search "textcite:" desc)))
               (format "\\textcite{%s}" path)
               (format "\\textcite[%s]{%s}" desc path)
  )))))
  
  (org-add-link-type 
     "autocite" 'ebib
     (lambda (path desc format)
       (cond
        ((eq format 'latex)
         (if (or (not desc) (equal 0 (search "autocite:" desc)))
               (format "\\autocite{%s}" path)
           (format "\\autocite[%s]{%s}" desc path)
  )))))
  
  (org-add-link-type 
   "footcite" 'ebib
   (lambda (path desc format)
     (cond
      ((eq format 'latex)
       (if (or (not desc) (equal 0 (search "footcite:" desc)))
           (format "\\footcite{%s}" path)
         (format "\\footcite[%s]{%s}" desc path)
         )))))
  
  (org-add-link-type 
   "fullcite" 'ebib
   (lambda (path desc format)
     (cond
      ((eq format 'latex)
       (if (or (not desc) (equal 0 (search "fullcite:" desc)))
           (format "\\fullcite{%s}" path)
         (format "\\fullcite[%s]{%s}" desc path)
         )))))
  
  (org-add-link-type 
   "citetitle" 'ebib
   (lambda (path desc format)
     (cond
      ((eq format 'latex)
       (if (or (not desc) (equal 0 (search "citetitle:" desc)))
           (format "\\citetitle{%s}" path)
         (format "\\citetitle[%s]{%s}" desc path)
         )))))
  
  (org-add-link-type 
   "citetitles" 'ebib
   (lambda (path desc format)
     (cond
      ((eq format 'latex)
       (if (or (not desc) (equal 0 (search "citetitles:" desc)))
           (format "\\citetitles{%s}" path)
         (format "\\citetitles[%s]{%s}" desc path)
         )))))
  
  (org-add-link-type 
     "headlessfullcite" 'ebib
     (lambda (path desc format)
       (cond
        ((eq format 'latex)
         (if (or (not desc) (equal 0 (search "headlessfullcite:" desc)))
               (format "\\headlessfullcite{%s}" path)
               (format "\\headlessfullcite[%s]{%s}" desc path)
  )))))   

(defun org-mode-reftex-setup ()
  (load-library "reftex")
  (and (buffer-file-name)
       (file-exists-p (buffer-file-name))
       (reftex-parse-all))
  (define-key org-mode-map (kbd "C-c )") 'reftex-citation))

(add-hook 'org-mode-hook 'org-mode-reftex-setup)



(provide 'ism-org-bib)