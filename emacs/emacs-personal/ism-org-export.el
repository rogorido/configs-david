(message "Orgmode-Exporteinstellungen werden geladen...")

(require 'org-latex)
;; Choose either listings or minted for exporting source code blocks.
;; Using minted (as here) requires pygments be installed. To use the
;; default listings package instead, use
;; (setq org-export-latex-listings t)
;; and change references to "minted" below to "listings"
(setq org-export-latex-listings 'minted)

;; default settings for minted code blocks
(setq org-export-latex-minted-options
      '(;("frame" "single")
;	("bgcolor" "bg") ; bg will need to be defined in the preamble of your document.
	("fontsize" "\\small")
	))

; no reconoce R sino como r
(add-to-list 'org-export-latex-minted-langs '(R "r"))

;; turn off the default toc behavior; deal with it properly in headers to files.
(defun org-export-latex-no-toc (depth)  
  (when depth
    (format "%% Org-mode is exporting headings to %s levels.\n"
	    depth)))
(setq org-export-latex-format-toc-function 'org-export-latex-no-toc)

(add-to-list 'org-export-latex-classes
	     '("koma-article"
	       "\\documentclass[paper=a4,fontsize=12pt,DIV12,headings=normal,listof=totoc]{scrartcl}"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{%s}" . "\\paragraph*{%s}")
	       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
))
    
;; Default packages included in /every/ tex file, latex, pdflatex or xelatex
(setq org-export-latex-packages-alist
      '(("" "graphicx" t)
	("" "longtable" nil)
	("" "paralist")))

(setq org-export-latex-default-packages-alist
      '(("" "ism-preamble-org" t)
))

;(load "ism-org-bib.el")

; hace falta para que haga todo el proceso de pdflatex-bibtex-etc.
; la -f significa 'force'
;(setq org-latex-to-pdf-process (list "latexmk -f -pdf %f"))

;------------------------------------------------------
; HTML
;------------------------------------------------------

; Inline images in HTML instead of producting links to the image
(setq org-export-html-inline-images t)
; Do not use sub or superscripts - I currently don't need this functionality in my documents
(setq org-export-with-sub-superscripts nil)
; Use org.css from the norang website for export document stylesheets
;(setq org-export-html-style-extra "<link rel=\"stylesheet\" href=\"http://doc.norang.ca/org.css\" type=\"text/css\" />")
(setq org-export-html-style-extra "<link rel=\"stylesheet\" href=\"css/org.css\" type=\"text/css\" />")
(setq org-export-html-style-include-default nil)
; Do not generate internal css formatting for HTML exports
(setq org-export-htmlize-output-type (quote css))
; Export with LaTeX fragments
(setq org-export-with-LaTeX-fragments t)
  
  ; List of projects
  ; basket: lo del baloncesto
  ; basket-extra: lo del baloncesto (imágenes, css)
  ; basket-general: lo del baloncesto (los dos anteriores juntos)
  
(setq org-publish-project-alist
      (quote (("basket"
	       :base-directory "~/latexpruebas/git/basket"
;	       :publishing-directory "/ssh:igor@192.168.1.101:~/public_html"
	       :publishing-directory "~/latexpruebas/public_html"
	       :recursive t
	       :base-extension "org"
	       :publishing-function org-publish-org-to-html
	       :style-include-default nil
	       :section-numbers nil
;	       :table-of-contents nil
;	       :style "<link rel=\"stylesheet\" href=\"grub.css\" type=\"text/css\" />"
	       :author-info nil
	       :creator-info nil)
	      ("basket-extra"
	       :base-directory "~/latexpruebas/git/basket"
;	       :publishing-directory "/ssh:igor@192.168.1.101:~/public_html"
	       :publishing-directory "~/latexpruebas/public_html"
	       :base-extension "css\\|png"
	       :publishing-function org-publish-attachment
	       :recursive t
	       :author nil)
	      ("basket-general"
	       :components ("basket" "basket-extra")))))

  
  ; I'm lazy and don't want to remember the name of the project to publish when I modify
  ; a file that is part of a project.  So this function saves the file, and publishes
  ; the project that includes this file
  ;
  ; It's bound to C-S-F12 so I just edit and hit C-S-F12 when I'm done and move on to the next thing
(defun bh/save-then-publish ()
  (interactive)
  (save-buffer)
  (org-save-all-org-buffers)
  (org-publish-current-project))
  
(global-set-key (kbd "<f9> x") 'bh/save-then-publish)


(add-to-list 'org-latex-classes
  '("djcb-org-article"
"\\documentclass[11pt,a4paper]{article}
\\usepackage[T1]{fontenc}
\\usepackage{fontspec}
\\usepackage{graphicx} 
\\defaultfontfeatures{Mapping=tex-text}
\\setromanfont{Gentium}
\\setromanfont [BoldFont={Gentium Basic Bold},
                ItalicFont={Gentium Basic Italic}]{Gentium Basic}
\\setsansfont{Charis SIL}
\\setmonofont[Scale=0.8]{DejaVu Sans Mono}
\\usepackage{geometry}
\\geometry{a4paper, textwidth=6.5in, textheight=10in,
            marginparsep=7pt, marginparwidth=.6in}
\\pagestyle{empty}
\\title{}
      [NO-DEFAULT-PACKAGES]
      [NO-PACKAGES]"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


(provide 'ism-org-export)
