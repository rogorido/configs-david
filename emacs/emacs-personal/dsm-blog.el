(setq load-path (cons "~/packages-emacs/org2blog/" load-path))
(require 'org2blog-autoloads)

(setq org2blog/wp-blog-alist
      '(("ciudad de piedra"
         :url "https://ciudaddepiedra.wordpress.com/xmlrpc.php"
         :username "kapshagai"
         :default-title "Nueva entrada")
	))
