(message "Cargando configuraciones generales...")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-quote-language-alist (quote (("spanish" "\"`" "\"'" t))))
 '(TeX-source-specials-mode 1)
 '(case-fold-search t)
 '(current-language-environment "UTF-8")
 '(default-input-method "rfc1345")
 '(font-latex-fontify-sectioning 1.05)
 '(global-font-lock-mode t nil (font-lock))
 '(mouse-wheel-mode t nil (mwheel))
 '(safe-local-variable-values
   (quote
    ((moe . org)
     (dired-omit-mode . t)
     (TeX-master . t))))
 '(sentence-end "[.?!][]\"')]*\\($\\|	\\| \\)[
]*")
 '(sentence-end-double-space nil)
 '(session-use-package t nil (session)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
