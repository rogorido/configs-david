(message "Dired+ wird geladen...")

; para el problema que da ido al rename/mover
; solo hay que pulsar C-j en el directorio al que
; quiero mover el fichero

; al parecer esto tiene que ir después de configurar lo
; de omit-files!
;(require 'dired+)

(setq dired-recursive-copies 'always
      dired-recursive-deletes 'top
      dired-dwim-target t
      dired-omit-files-p t
      dired-omit-mode t
;      dired-omit-files (concat dired-omit-files "\\|\\.out$\\|\\.bib$\\|\\.log$\\|^\\..+$")
;   dired-actual-switches "-la"
      dired-listing-switches "-lah --group-directories-first"
; estos tres no sé qué son...
;        dired-recursive-deletes 'top
;        dired-recursive-copies 'always
;        ls-lisp-use-insert-directory-program nil
;        ls-lisp-dirs-first t ; esto no funciona
      ls-lisp-ignore-case t
)

; con esto es posible abrir directorios en dired
; con a sin que abra un nuevo puto frame...
;(put 'dired-find-alternate-file 'disabled nil)
;(toggle-dired-find-file-reuse-dir 1)


(setq delete-by-moving-to-trash t
      trash-directory "~/.Trash/emacs")

;; (eval-after-load "dired+"
;;   '(progn
;;      (define-key dired-mode-map  "\177" 'dired-up-directory)
;; ;     (define-key dired-mode-map  "j" 'dired-next-line)
;; ;     (define-key dired-mode-map  "k" 'dired-previous-line)
;; ))

;(add-to-list 'load-path "~/abs/dired-sort")
;(require 'dired-sort)

; iluminar la línea de dired
(add-hook 'dired-mode-hook (lambda ()
     (hl-line-mode 1)))

; increíble: con esto se consigue que en el modo agenda
; el ratón no 'ilumine' la línea sobre la que está...
; no funciona... ¿por qué? sí funciona con org-agenda...
(add-hook 'dired-mode-hook
    (lambda () (remove-text-properties
               (point-min) (point-max) '(mouse-face t))))

;; (add-hook 'dired-mode-hook
;; '(lambda ()
;;    (define-key dired-mode-map "e" 'wdired-change-to-wdired-mode)
;;    ))

;; (setq dired-guess-shell-alist-user
;;         '(("\\.xls\\'" "soffice")
;;           ("\\.ods\\'" "soffice")
;;           ("\\.odt\\'" "soffice")
;; 	  ("\\.mkv\\'" "mpv ? ;&")
;; 	  ("\\.avi\\'" "mpv ? ;&")
;; 	  ("\\.wmv\\'" "mpv ? ;&")
;; 	  ("\\.ogv\\'" "mpv ? ;&")
;; 	  ("\\.mpg\\'" "mpv ? ;&")
;; 	  ("\\.mp3\\'" "mpv ? ;&")
;; 	  ("\\.ogg\\'" "mpv ? ;&")
;; 	  ("\\.zip\\'" "7z x ? ;&")
;; 	  ("\\.rar\\'" "7z x ? ;&")
;; 	  ("\\.7z\\'" "7z x ? ;&")
;; 	  ("\\.tar.gz\\'" "tar xvf ? ;&")
;; 	  ("\\.tar.bz\\'" "tar xvf ? ;&")))

; cogido de
; http://www.emacswiki.org/emacs/DiredOmitMode
;; (defun ism/dired-dotfiles-toggle ()
;;     "Show/hide dot-files"
;;     (interactive)
;;     (when (equal major-mode 'dired-mode)
;;       (if (or (not (boundp 'dired-dotfiles-show-p)) dired-dotfiles-show-p) ; if currently showing
;; 	  (progn 
;; 	    (set (make-local-variable 'dired-dotfiles-show-p) nil)
;; 	    (message "h")
;; 	    (dired-mark-files-regexp "^\\\.")
;; 	    (dired-do-kill-lines))
;; 	(progn (revert-buffer) ; otherwise just revert to re-show
;; 	       (set (make-local-variable 'dired-dotfiles-show-p) t)))))

; ATENCIÓN: con emacs 24.4 parece ser
; que no va a hacer falta cargar esto!!
;(require 'dired-details)
;(dired-details-install)

;(setq dired-details-hidden-string "")

;; (add-to-list 'load-path "~/giteando/dired-hacks")
;; (require 'dired-filter)
;; (require 'dired-ranger)
;; (require 'dired-hacks-utils)


(provide 'ism-dired)
