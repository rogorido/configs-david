(message "Cargando la configuración de undo...")

(require 'undo-tree)

(define-key undo-tree-visualizer-mode-map (kbd "q") 'undo-tree-visualizer-abort)
(define-key undo-tree-visualizer-mode-map [return] 'undo-tree-visualizer-quit)

(global-undo-tree-mode 1)
