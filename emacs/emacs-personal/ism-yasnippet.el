(message "Cargando las plantillas de autocompletado (yasnippet)...")

;(add-to-list 'load-path "~/.emacs.d/elpa/yasnippet-0.8.0")
;(add-to-list 'load-path "~/.emacs.d/elpa/yasnippet-20140314.255")
;(add-to-list 'load-path "/usr/share/emacs/site-lisp/yas")

(require 'yasnippet) ;; not yasnippet-bundle
;(yas-global-mode 1)

(add-hook 'org-mode-hook 'yas-minor-mode-on)
(add-hook 'mail-mode-hook 'yas-minor-mode-on)
(add-hook 'message-mode-hook 'yas-minor-mode-on)

(setq yas/root-directory '(;"~/.emacs.d/elpa/yasnippet-20140314.255/snippets"
                           "~/Documents/snippets"))

;; Map `yas/load-directory' to every element
(mapc 'yas/load-directory yas/root-directory)

; cambiamos TAB por C-ñ
; pues si no, hay muchos conflictos
; esto parece que ya no funciona...
;(setq yas/trigger-key "C-ñ")
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
(define-key yas-minor-mode-map (kbd "C-ñ") 'yas-expand)

(provide 'ism-yasnippet)
