(message "Cargando configuraciones de diccionarios...")

;; con esto se activa aspell como programa de ortografía en lugar de ispell
(setq-default ispell-program-name "aspell")

;;con esto se arranca el modo flyspell para los modos de texto y parecidos (también latex)
;(add-hook 'text-mode-hook 'flyspell-mode)
;(setq flyspell-issue-welcome-flag nil)

;;todo estas 3 siguientes cargan los diccionarios...
(setq ispell-dictionary "castellano8"
      ispell-local-dictionary "castellano8"
      flyspell-default-dictionary "castellano8"
      ispell-highlight-face 'highlight
      ispell-silently-savep t)

; atención en ism-latex.el tengo una configuración para
; que se salte cosas de latex

(provide 'ism-aspell)
