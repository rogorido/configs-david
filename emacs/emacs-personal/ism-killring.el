(message "Kill-Ring-Einstellungen werden geladen...")

;(add-to-list 'load-path "~/giteando/browse-kill-ring")
(require 'browse-kill-ring)
; con esto se usan las defaults
;(browse-kill-ring-default-keybindings)
(setq browse-kill-ring-highlight-current-entry t)
(setq browse-kill-ring-display-duplicates t)

; lo tengo vinculado a "C-c y" en ism-tasten.el

;(add-to-list 'load-path "~/giteando/emacs-mainline")
;(require 'main-line)
;(setq main-line-separator-style 'wave)

(setq browse-kill-ring-highlight-inserted-item t)

(provide 'ism-killring)
