(message "Andere Einstellungen werden geladen...")

; esto me parece un poco complicado... pero parece que funciona...
(defun define-trivial-mode(mode-prefix file-regexp &optional command)
  (or command (setq command mode-prefix))
  (let ((mode-command (intern (concat mode-prefix "-mode"))))
    (fset mode-command
          `(lambda ()
             (interactive)
             (toggle-read-only t)
	     (start-process ,mode-prefix nil
			    ,command (buffer-file-name))
	     (let ((obuf (other-buffer (current-buffer) t)) ;; Select correct buffer
		   (kbuf (current-buffer)))
	       (set-buffer obuf)			    ;; set it as current
	       (kill-buffer kbuf))))			    ;; kill temporary buffer
    (add-to-list 'auto-mode-alist (cons file-regexp mode-command))))

;(define-trivial-mode "ghostview" "\\.ps$")
(define-trivial-mode "zathura" "\\.pdf$")
(define-trivial-mode "zathura" "\\.PDF$")
(define-trivial-mode "djview" "\\.djvu$")
(define-trivial-mode "soffice" "\\.doc$")
(define-trivial-mode "soffice" "\\.odt$")


; con esto se logra que no meta en el trash algunos ficheros temporales
(setq system-trash-exclude-matches '("#[^/]+#$" ".*~$" "\\.emacs\\.desktop.*" "#.+"))
(setq system-trash-exclude-paths '("/tmp"))

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))


; esto es para el kill-ring
; de http://www.emacswiki.org/emacs/popup-kill-ring.el
;(require 'popup)
;(require 'pos-tip)
;(require 'popup-kill-ring)
; atención esto está puesto en ism-tasten.el para que esté todo 
; junto sobre las tasten
;(global-set-key "\M-y" 'popup-kill-ring)


(require 'pkgbuild-mode)
(setq auto-mode-alist (append '(("/PKGBUILD$" . pkgbuild-mode)) auto-mode-alist))

;; make buffer names easily identifiable
(require 'uniquify) ; bundled with GNU Emacs 23.2.1 or earlier
(setq uniquify-buffer-name-style 'post-forward)

; permite 'empotrar' speedbar en el frame y que no esté flotando
;(require 'sr-speedbar)

;(add-to-list 'load-path "~/abs/sauron")
;(require 'sauron)


(provide 'ism-misc)
