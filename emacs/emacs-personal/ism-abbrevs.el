(message "Cargando el sistema de abreviaturas...")

;; save abbrevs when files are saved
;; you will be asked before the abbreviations are saved
(setq save-abbrevs t)

;; reads the abbreviations file on startup
(quietly-read-abbrev-file)

;;para que active el modo de las abreviaciones
(abbrev-mode 1)
(setq default-abbrev-mode t)

(provide 'ism-abbrevs)
