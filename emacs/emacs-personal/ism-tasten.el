(message "Cargando la configuración de teclas del davidiano...")

(global-set-key [f1] 'delete-other-windows)
(global-set-key [f2] 'split-window-vertically)
(global-set-key [f3] 'split-window-horizontally)
(global-set-key [f4] 'kill-buffer)

; cosas de latex
;(global-set-key [f7] 'TeX-fold-buffer)
;(global-set-key [f8] 'reftex-citation)
;(global-set-key [f9] 'anything-for-files)
;(global-set-key [f11] 'TeX-PDF-mode)
;(global-set-key [f12] 'TeX-command-master)

; helm-swoop por i-search
(global-set-key "\C-s" 'helm-swoop)

(global-set-key [(control +)] 'text-scale-increase)
(global-set-key [(control -)] 'text-scale-decrease)

; funciones generales
(global-set-key (kbd "s-a") 'org-agenda)
(global-set-key (kbd "s-f") 'helm-buffers-list)
(global-set-key (kbd "s-b") 'hydra-general-blog/body)
(global-set-key (kbd "s-g") 'hydra-general-total/body)
(global-set-key (kbd "s-c") 'org-capture)
(global-set-key (kbd "s-x") 'helm-imenu)
(global-set-key (kbd "s-h") 'cheatsheet-show)
;(global-set-key (kbd "s-l") 'flyspell-auto-correct-previous-word)

(global-set-key "\M-1" 'save-buffer)
(global-set-key "\M-2" 'dsm/abrir-general-blog-turquistan)
(global-set-key "\M-3" 'dsm/abrir-general-blog-piedra)
(global-set-key "\M-4" 'dsm/abrir-privado)

;(global-set-key "\M-\S-k" 'ism/borrar-frase)

;; (global-set-key (kbd "<f9> a") 'org-agenda)
;; (global-set-key (kbd "<f9> <f9>") 'org-agenda)
;; (global-set-key (kbd "<f9> d") 'ism/org-agenda-heute)
;; (global-set-key (kbd "<f9> e") 'ism/org-capture-e)  ;email persönlich
;; (global-set-key (kbd "<f9> E") 'ism/org-capture-arbeit) ; email arbeit
;; ;(global-set-key (kbd "<f9> f") 'ism/org-capture-freiburg)
;; (global-set-key (kbd "<f9> i") 'ibuffer)
;; (global-set-key (kbd "<f9> p") 'ism/org-capture-palabra)
;; (global-set-key (kbd "<f9> r") 'org-capture)
;; (global-set-key (kbd "<f9> R") 'helm-org-capture-templates)
;; (global-set-key (kbd "<f9> s") 'org-narrow-to-subtree)
;; (global-set-key (kbd "<f9> t") 'ism/org-capture-todo)
;; (global-set-key (kbd "<f9> w") 'widen)
;; (global-set-key (kbd "<f9> c") 'cfw:open-calendar-buffer)
;; (global-set-key (kbd "<f9> C") 'cfw:open-org-calendar)

;(global-set-key (kbd "<f9> I") 'bh/clock-in)
;(global-set-key (kbd "<f9> O") 'bh/clock-out)
;(global-set-key (kbd "<f9> SPC") 'bh/clock-in-last-task)

; primero deshabilitamos f10...
(global-unset-key (kbd "<f10>"))
(global-set-key (kbd "<f10> b") 'hydra-general-blog/body)
(global-set-key (kbd "<f10> g") 'hydra-general-total/body)
(global-set-key (kbd "<f10> c") 'org-capture)

; deshabilitar C-z porque realmente para qué coño sirve...
(global-unset-key "\C-z")
(global-set-key (kbd "C-z") 'undo-tree-visualize)
(global-set-key (kbd "C-x u") 'undo-tree-visualize)

; para el popup-kill-ring
;(global-set-key "\M-y" 'popup-kill-ring)

;; ...never switch to overwrite mode, not even accidentally
(global-set-key [insert]
  (function
   (lambda () (interactive)
     (message "Lo siento, el modo de sobreescritura lo he deshabilitado"))))

;;----------------------------------------------------
;; Esto sirve para hacer C+a marcar todo el buffer
;;----------------------------------------------------

;(global-set-key "\C-a" 'mark-whole-buffer)

(eval-after-load "org-agenda" '(define-key org-agenda-mode-map "\M-+"
			  'org-agenda-date-later))

(eval-after-load "org-agenda" '(define-key org-agenda-mode-map "\M--"
			  'org-agenda-date-earlier))

(global-set-key "\C-cy" 'browse-kill-ring)


;(global-set-key [(hyper q)] 'ism/ir-a-gnus)
;(global-set-key [(hyper w)] 'ism/ir-a-agenda)
(global-set-key (kbd "H-¡") 'ism/ir-a-agenda)
;(global-set-key [(hyper tab)] 'helm-buffers-list)
(global-set-key (kbd "H-0") 'helm-buffers-list)
;(global-set-key [(hyper j)] 'ism/ir-a-jabber)
(global-set-key (kbd "H-'") 'ism/ir-a-jabber)
(global-set-key [(hyper r)] 'ism/ir-a-r)
(global-set-key [(hyper i)] 'ism/ir-a-ibuffer)
(global-set-key [(hyper m)] 'mu4e)
(global-set-key [(hyper b)] 'helm-bookmarks)
(global-set-key [(hyper q)] 'ism/switch-diccionario)
(global-set-key [(hyper x)] 'helm-imenu)
(global-set-key [(hyper +)] 'calculator)
(global-set-key [(hyper .)] 'ace-window)
(global-set-key [(hyper l)] 'flyspell-auto-correct-previous-word)
;(global-set-key [(hyper -)] 'flyspell-auto-correct-previous-word)
;(global-set-key [(hyper -)] 'flyspell-correct-word-before-point)
(global-set-key [(hyper -)] 'helm-flyspell-correct)
; pongo por ahora dos...
(global-set-key [(hyper ?\ñ)] 'hydra-general/body)



(provide 'ism-tasten)
