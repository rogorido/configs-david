(setq org-capture-templates
      '(
	("e" "email nuevo (personal)" entry (file+headline "~/Escritorio/general/org/privado.org" "Emails")
	 "* TODO escribir a %?\n SCHEDULED: %t ")

	("r" "responder privado" entry (file+headline "~/Escritorio/general/org/privado.org" "Emails")
	 "* TODO Responder a %:fromname \nSCHEDULED: %t\nTema:%:subject\n%a\n" )

	;; TODOs
	("t" "TODO privado" entry (file+headline "~/Escritorio/general/org/privado.org" "General")
	 "* TODO %?\n SCHEDULED: %t ")
					; nueva palabra
	;("p" "wort" plain (file "~/Documents/sprachen/espanol.txt")
	; "%^{wort}" :empty-lines 1 :immediate-finish t)
	
	;; leer 
	("l" "leer" entry (file+headline "~/Escritorio/general/org/estudiar.org" "Leer")
	 "* LEER %? %^g\n %^{Effort}p\n %x")
	
	;; cosas variadas para aprender
	("b" "biblio para blog turquistán" entry (file+headline "~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org" "Biblio" )
	 "* LEER %? %^g\n %^{Effort}p\n %x")

	("B" "biblio para blog ciudad de piedra" entry (file+headline "~/Escritorio/general/blogs/blog-piedra/blog-general-piedra.org" "Biblio" )
	 "* LEER %? %^g\n %^{Effort}p\n %x")

	))

