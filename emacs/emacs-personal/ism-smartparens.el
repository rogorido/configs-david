(message "Cargando configuración de paréntesis...")

(require 'smartparens-config)

; mejor no, pues ocupa también la org-agenda!
;(smartparens-global-mode t)

;(smartparens-global-mode nil)

(add-hook 'prog-mode-hook 'smartparens-mode)
(add-hook 'text-mode-hook 'smartparens-mode)

;(sp-pair "¿" "?")
;(sp-pair "¡" "!")

; esto sirva para que no añade un paréntesis de cierre si es
; está justo antes de palabra
(sp-pair "(" nil :unless '(sp-point-before-word-p))
(sp-pair "¡" nil :unless '(sp-point-before-word-p))
(sp-pair "¿" nil :unless '(sp-point-before-word-p))
(sp-pair "{" nil :unless '(sp-point-before-word-p))
(sp-pair "'" nil :unless '(sp-point-before-word-p))
(sp-pair "\"" nil :unless '(sp-point-before-word-p))
(sp-pair "[" nil :unless '(sp-point-before-word-p))

(provide 'ism-smartparens)
