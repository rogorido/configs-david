(message "Cargando las funciones propias del paspancín...")

(defun dsm/buscar-en-blogs ()
  "Buscar con helm en las entradas del blog"
  (interactive)
  (helm-do-grep-1 '("~/Escritorio/general/blogs/blog-turquistan/entradas/*.org"
		    "~/Escritorio/general/blogs/blog-piedra/entradas/*.org")))

(defun dsm/abrir-general-blog-turquistan ()
  "Abrir el fichero general del blog turquistán"
  (interactive)
  (find-file "~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org")
  )

(defun dsm/abrir-general-blog-piedra ()
  "Abrir el fichero general del blog piedra"
  (interactive)
  (find-file "~/Escritorio/general/blogs/blog-piedra/blog-general-piedra.org")
  )

(defun dsm/abrir-privado ()
  "Abrir el fichero privado.org"
  (interactive)
  (find-file "~/Escritorio/general/org/privado.org")
  )

(defun dsm/añadir-tarea-blog ()
  "Añadir una tarea al blog."
  (interactive)
  
  )

(defun dsm/idea-bruto-blog ()
  "Añadir una idea en bruto al blog."
  (interactive)
  
  )

(defun dsm/agenda-blog ()
  "Mostrar una agenda con los TODOs del blog."
  )

(defun dsm/publicar-entrada-blog ()
  (interactive)
  (let ((current-prefix-arg 4)) ;; emulate C-u
    (call-interactively 'org2blog/wp-post-buffer)
    )
  )

(defun dsm/abrir-fichero-ayuda ()
  "Abrir el fichero general del blog turquistán"
  (interactive)
  (find-file "~/configs-david/emacs/notasemacs.org")
  )

(defun dsm/activar-modo-lineas-blog ()
  "Activar el modo de líneas propio del blog."
  (interactive)
  (turn-off-auto-fill)
  (longlines-mode)
  )
