(message "Cargando las configuraciones de correo...")

(require 'mu4e)
(require 'org-mu4e)

;; when mail is sent, automatically convert org body to HTML
(setq org-mu4e-convert-to-html t)

(setq user-full-name "David Sosa Mayor")
(setq user-mail-address "davidsosa@zoho.com")

;; Only needed if your maildir is _not_ ~/Maildir
(setq mu4e-maildir "~/Mail")
(setq mu4e-drafts-folder "/Drafts"
      mu4e-sent-folder   "/Sent"
      mu4e-trash-folder  "/Trash")

(setq mu4e-compose-reply-to-address "davidsosa@zoho.com")

(setq mu4e-user-mail-address-list '("davidsosa@zoho.com" "davidsosa@qq.com"))

;(setq mu4e-get-mail-command "fetchmail -f ~/.fetchgmail")
(setq mu4e-get-mail-command "true")

; sirve para que guarde el email queue...
(setq smtpmail-queue-dir "~/Mail/queue/")

(setq mu4e-compose-complete-only-personal t)
; esto mata el buffer al salir
(setq message-kill-buffer-on-exit t)
;(setq mu4e-update-interval 300)

(setq mu4e-headers-show-threads nil
      mu4e-headers-results-limit 70
;      mu4e-compose-signature-auto-include nil ; quita ese signatura propia que pone...
      mu4e-compose-signature-auto-include nil ; quita ese signatura propia que pone...
      mu4e-view-show-addresses t
      mu4e-view-show-images t
;      mu4e-view-wrap-lines nil
      mu4e-use-fancy-chars t)

(setq mu4e-confirm-quit nil)

;(setq mu4e-headers-sort-revert nil)

;(setq mu4e-view-prefer-html t)
;(setq mu4e-html2text-command "w3m")
;(setq mu4e-html2text-command "w3m -dump -cols 80 -T text/html") 
;(setq mu4e-html2text-command "html2text -utf8 -width 72")


; la última t sirve para que lo ponga al final de la lista
;(add-to-list 'mu4e-bookmarks '("size:5M..500M" "Big messages" ?b) t)
;(add-to-list 'mu4e-bookmarks '("mime:application/pdf" "Pdf attachments" ?P) t)
       
(setq mu4e-maildir-shortcuts
      '( ("/INBOX"      . ?i)
	 ("/Sent"       . ?s)
	 ("/Trash"      . ?t)
	 ("/Spam"       . ?m)
	 ("/Drafts"     . ?d)))

;; allow for updating mail using 'U' in the main view:
(setq mu4e-get-mail-command "offlineimap")

(setq mu4e-headers-fields
         '( (:date          .  25)
            (:flags         .   6)
            (:from-or-to    .  30)
            (:subject       .  nil)))

;(setq mu4e-attachment-dir (file-name-expand "~/latexpruebas"))

;(add-hook 'mu4e-compose-mode-hook 'mu4e-change-user-based-on-maildir)
;(add-hook 'mu4e-compose-mode-hook (lambda ()                    
;                               (setq auto-fill-function org-auto-fill-function
;                                )))

;(add-hook 'mu4e-compose-mode-hook 'auto-complete-mode)

(add-hook 'mu4e:main
 (lambda ()
 (local-set-key (kbd "m") 'mu4e-compose-new)
 (define-key mu4e-main-mode-map "m" 'mu4e-compose-new)
 )
)

; al parecer mu4e no usa como estandar lo de:
; On %a, %b %d %Y, %N wrote:\n"
; con esto se consigue...
;(setq message-citation-line-function
;          'messages-insert-formatted-citation-line)

(defun my-mu4e-choose-from ()
  "Choose the From field from user mail address list"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (not (looking-at "From: ")) (forward-line))
    (kill-line)
    (insert (concat "From: " user-full-name " <>"))
    (backward-char)
    (insert
     (ido-completing-read "From: " mu4e-user-mail-address-list))))

(define-key message-mode-map (kbd "C-c C-f f") 'my-mu4e-choose-from)

(setq mu4e-headers-seen-mark '("S" . "☑")
      mu4e-headers-new-mark '("N" . "✉")
      mu4e-headers-replied-mark '("R" . "↵")
      mu4e-headers-passed-mark '("P" . "⇉")
      mu4e-headers-encrypted-mark '("x" . "⚷")
      mu4e-headers-signed-mark '("s" . "✍")
      mu4e-headers-empty-parent-prefix '("-" . "○")
      mu4e-headers-first-child-prefix '("\\" . "━▶")
      mu4e-headers-has-child-prefix '("+" . "●")
      mu4e-headers-default-prefix '("|" . "○"))

(require 'org-mu4e)
;(mu4e-maildirs-extension)

;; (add-hook 'mu4e-view-mode-hook
;;   (lambda()
;;      ;; try to emulate some of the eww key-bindings
;;     (local-set-key (kbd "<tab>") 'shr-next-link)
;;     (local-set-key (kbd "<backtab>") 'shr-previous-link)))

;(require 'bbdb-loaddefs) 
;; (autoload 'bbdb-insinuate-mu4e "bbdb-mu4e")
;; (bbdb-initialize 'message 'mu4e 'gnus)
;; (setq bbdb-mail-user-agent (quote message-user-agent))
;; (setq mu4e-view-mode-hook (quote (bbdb-mua-auto-update visual-line-mode)))
;; (setq mu4e-compose-complete-addresses nil)

(require 'smtpmail)

; y esto para que los mensajes salga codificados bien...
(setq mm-coding-system-priorities '(iso-8859-1 iso-8859-15 utf-8))

; la auth está en .authinfo
;; (setq message-send-mail-function 'smtpmail-send-it
;;       smtpmail-stream-type 'starttls
;;       smtpmail-default-smtp-server "smtp.zoho.com"
;;       smtpmail-smtp-server "smtp.zoho.com"
;;       smtpmail-smtp-service 587)

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'ssl
      smtpmail-default-smtp-server "smtp.zoho.com"
      smtpmail-smtp-server "smtp.zoho.com"
      smtpmail-smtp-service 465)

(setq password-cache-expiry 86400)      ; Time (in sec) to cache SMTP password

; authinfo.gpg está encriptado
; se explica muy fácil: http://www.emacswiki.org/emacs/GnusEncryptedAuthInfo
; en teoría hay que poner lo de require 'epa-file
; pero yo no lo tengo puesto y funciona...

; la estructura de .authinfo es:
;machine smtp.gmail.com login igor.sosa@gmail.com password MICONTRA port 587
;machine listas login joseleopoldo1792@gmail.com password MICONTRA port 993

(defun ism-mail-mode-hook ()
  (turn-on-auto-fill) ;;; Auto-Fill is necessary for mails
  (setq default-justification 'left)
  (setq fill-column 72)
  (turn-on-font-lock) ;;; Font-Lock is always cool *g*
;  (flush-lines "^\\(> \n\\)*> -- \n\\(\n?> .*\\)*")
                      ;;; Kills quoted sigs.
;  (not-modified)      ;;; We haven't changed the buffer, haven't we? *g*
;  (mail-text)         ;;; Jumps to the beginning of the mail text
  (setq make-backup-files nil)   ;;; No backups necessary.
)

(setq message-citation-line-function 'message-insert-formatted-citation-line
      message-citation-line-format "\n%f skribis:\n")

(add-hook 'mail-mode-hook 'turn-on-flyspell 'append)
(add-hook 'message-mode-hook 'turn-on-flyspell 'append)
(add-hook 'mu4e-compose-mode-hook 'turn-on-flyspell 'append)
(add-hook 'mu4e-compose-mode-hook 'ism-mail-mode-hook)

(setq mu4e-update-interval 900)

; con esto hacemos que por defecto todo se envíe como attachment
; y no como inline
(setq mml-content-disposition-alist '((t . "attachment")))

;; con esto si pulsamos aV en un mensaje
;; lo abre en el browser
(add-to-list 'mu4e-view-actions
             '("ViewInBrowser" . mu4e-action-view-in-browser) t)

(provide 'dsm-mu4e)
