(message "Eigene Funktionen werden geladen...")

; esto define una función para saber si existe un buffer
; y además  borra el jodido scratch
(defun buffer-exists (bufname) (not (eq nil (get-buffer bufname))))
;; (if (buffer-exists "*scratch*")
;;     (kill-buffer "*scratch*"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun art ()
  "Iniciar un documento articulo de latex"
  (interactive)
  (insert-file-contents "~/.emacs.d/artikel.tex"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun presenta ()
  "Iniciar un documento de beamer de latex"
  (interactive)
  (insert-file-contents "~/.emacs.d/presentacion.tex"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun carta ()
  "Iniciar una carta en español"
  (interactive)
  (insert-file-contents "~/.emacs.d/carta.tex"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun briefe ()
  "Iniciar una carta en alemán"
  (interactive)
  (insert-file-contents "~/.emacs.d/brief.tex"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun zsfassung ()
  "Iniciar un resumen de un artículo"
  (interactive)
  (insert-file-contents "~/.emacs.d/zsfassung.tex"))

;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun ism/artikel ()
  "Iniciar un resumen de un artículo"
  (interactive)
  (insert-file-contents "~/.emacs.d/artikel.tex"))

(defun ism/artikel-memoir ()
  "Iniciar un resumen de un artículo"
  (interactive)
  (insert-file-contents "~/programacion/latex/memoir-general.tex"))

;;----------------------------------------------------
;;esto es para lo de las vorlagen
;;creo diversas funciones que introducen los documentos
;;----------------------------------------------------

(defun transcrip ()
  "Iniciar una transcripción"
  (interactive)
  (insert-file-contents "~/.emacs.d/transcripcion.tex"))

;;----------------------------------------------------
;; abrir thesis.org
;;----------------------------------------------------

(defun thesis ()
  "Datei thesis.org öffnen"
  (interactive)
  (find-file "~/Documents/org/thesis.org"))

;;----------------------------------------------------
;; abrir notes.org
;;----------------------------------------------------

(defun notes ()
  "Datei notes.org öffnen"
  (interactive)
  (find-file "~/Documents/org/notes.org"))

;;----------------------------------------------------
;; abrir notes.org
;;----------------------------------------------------

(defun privat ()
  "Datei privat.org öffnen"
  (interactive)
  (find-file "~/Documents/org/privat.org"))

;;----------------------------------------------------
;; abrir cosaslinux.org
;;----------------------------------------------------

(defun cosaslinux ()
  "Datei cosaslinux.org öffnen"
  (interactive)
  (find-file "~/Documents/cosaslinux.org"))

;;----------------------------------------------------
;; abrir espanol.txt
;;----------------------------------------------------

(defun ism/espanol ()
  "Datei espanol.txt öffnen"
  (interactive)
  (find-file "~/Documents/sprachen/espanol.txt"))

;;----------------------------------------------------
;; abrir deutsch.txt
;;----------------------------------------------------

(defun ism/deutsch ()
  "Datei deutsch.txt öffnen"
  (interactive)
  (find-file "~/Documents/sprachen/deutsch.txt"))

;;----------------------------------------------------
;; abrir griechisch.txt
;;----------------------------------------------------

(defun ism/griechisch ()
  "Datei griechisch.txt öffnen"
  (interactive)
  (find-file "~/Documents/sprachen/griechisch.txt"))

;;----------------------------------------------------
;; abrir esperanto.txt
;;----------------------------------------------------

(defun ism/esperanto ()
  "Datei esperanto.txt öffnen"
  (interactive)
  (find-file "~/Documents/sprachen/esperanto.txt"))


;;----------------------------------------------------
;; abrir cosaslinux.org
;;----------------------------------------------------

(defun erfurt-inhalt ()
  "Datei inhalt.org öffnen"
  (interactive)
  (find-file "~/geschichte/erfurt/konsum/inhalt.org"))

;;----------------------------------------------------
;; abrir emacs
;;----------------------------------------------------

(defun configemacs ()
  "Datei .emacs öffnen"
  (interactive)
  (find-file "~/.emacs"))

;;----------------------------------------------------
;; abrir reflexiones.tex
;;----------------------------------------------------

(defun reflexiones ()
  "Datei reflexiones öffnen"
  (interactive)
  (find-file "~/geschichte/konfessiodiss/text/reflexiones.tex"))

;;----------------------------------------------------
;; esto es para introducir refs en los docs org
;;----------------------------------------------------

(defun bibtex-insertar ()
  "Insertar una referencia en orgmode"
  (interactive)
  (find-file "~/Documents/bibliogeneral.bib")
  (call-interactively 'isearch-forward-regexp))

;;----------------------------------------------------
;; saltar entre buffers
;;----------------------------------------------------

; con esto guardamos en una variable el buffer donde estamos
(setq ism/nombre-del-buffer nil)

(defun ism/switch-buffer-determinado (ventana)
  "Función general para ir a un buffer y volver"
  ; la función buffer-exists la defino arriba
  (if (equal (buffer-name) ventana)
      (switch-to-buffer ism/nombre-del-buffer)
    (progn
      (setq ism/nombre-del-buffer (buffer-name))
      (switch-to-buffer ventana))))
       

(defun ism/ir-a-jabber ()
  "Ir al buffer de jabber"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*-jabber-roster-*"))


(defun ism/ir-a-agenda ()
  "Ir al buffer de agenda"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*Org Agenda*"))


(defun ism/ir-a-r ()
  "Ir al buffer de R"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*R*"))

(defun ism/gnus-switch-to-group-buffer ()
  "Switch to gnus group buffer if it exists, otherwise start gnus."
  (interactive)
  (if (and (fboundp 'gnus-alive-p)
           (gnus-alive-p))
      (switch-to-buffer gnus-group-buffer)
    (gnus)))

(defun ism/ir-a-gnus ()
  "Ir al buffer de gnus"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*Group*"))

(defun ism/ir-a-calfw ()
  "Ir al buffer de calendario"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*cfw-calendar*"))

(defun ism/ir-a-ibuffer ()
  "Ir al buffer de ibuffer"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*Ibuffer*"))

(defun ism/ir-a-emms-playlist ()
  "Ir al buffer de emms"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*EMMS-Playlist*"))

;; (defun ism/ir-a-twitter ()
;;   "Ir al buffer de twitter"
;;   (interactive)
;;   ; la función buffer-exists la defino arriba
;;   (ism/switch-buffer-determinado ":home"))

(defun ism/ir-a-mu4e ()
  "Ir al buffer de mu4e"
  (interactive)
  ; la función buffer-exists la defino arriba
  (ism/switch-buffer-determinado "*mu4e-main*"))

(defun ism/switch-diccionario ()
  "Cambiar de un diccionario a otro"
  (interactive)
  (if (equal ispell-current-dictionary "castellano8")
      (progn 
	(ispell-change-dictionary "deutsch8")
	(ism/cambiar-completar "alemán"))
    (progn
      (ispell-change-dictionary "castellano8")
      (ism/cambiar-completar "español"))))


(defun ism/org-agenda-heute ()
"Ir a la agenda del día"
  (interactive)
  (org-agenda nil " "))

(defun ism/org-agenda-thesis ()
"Ir a la agenda del día"
  (interactive)
  (org-agenda nil "ta"))

(defun kill-other-buffers ()
      "Kill all other buffers."
      (interactive)
      (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

;; ;;----------------------------------------------------
;; ;; Esto es una verdadera maravilla: sirve para cambiar
;; ;; entre los buffers con SHIFT-TECLA CURSOR DERECHA o IZQUIERDA
;; ;;----------------------------------------------------

;; ;; Buffer wechseln wie click auf modeline, aber mit
;;      ;; tasten...
;;      (defun ska-previous-buffer ()
;;        "Hmm, to be frank, this is just the same as bury-buffer.
;;      Used to wander through the buffer stack with the keyboard."
;;        (interactive)
;;        (bury-buffer))

;;      (defun ska-next-buffer ()
;;        "Cycle to the next buffer with keyboard."
;;        (interactive)
;;        (let* ((bufs (buffer-list))
;;            (entry (1- (length bufs)))
;;            val)
;;          (while (not (setq val (nth entry bufs)
;;                   val (and (/= (aref (buffer-name val) 0)
;;                        ? )
;;                        val)))
;;            (setq entry (1- entry)))
;;            (switch-to-buffer val)))

;; ;; Buffer cycling like on modeline
;;        (define-key global-map [(alt right)] 'ska-next-buffer)
;;        (define-key global-map [(alt left)]  'ska-previous-buffer)

;;----------------------------------------------------
;; Esto es una verdadera maravilla: sirve para cambiar
;; entre los windows con SHIFT-TECLA CURSOR ARRIBA o ABAJO
;;----------------------------------------------------


;; Fenster rückwärts springen   
;;           (defun other-window-backward (n)
;;             "Select Nth previous window."
;;             (interactive "p")
;;             (other-window (- n)))

;; (global-set-key [(shift down)] 'other-window)
;; (global-set-key [(shift up)] 'other-window-backward)


; mio

(defun ism/org-capture-e ()
  "Crear un email personal"
   (interactive)
   (org-capture nil "ep"))

(defun ism/org-capture-arbeit ()
  "Crear un email de trabajo"
   (interactive)
   (org-capture nil "eb"))

(defun ism/org-capture-todo ()
  "Crear un tarea general"
   (interactive)
   (org-capture nil "tp"))

(defun ism/org-capture-palabra ()
  "Introducir una nueva palabra"
   (interactive)
   (org-capture nil "p"))

; correo
(defun ism/mail-privat ()
   (interactive)
   (setq user-mail-address "igor.sosa@gmail.com")
   (setq message-sendmail-extra-arguments '("-a" "default"))
   (compose-mail))

(defun ism/mail-arbeit ()
   (interactive)
   (setq user-mail-address "Igor.Sosa@eui.emea.microsoftonline.com")
   (setq message-sendmail-extra-arguments '("-a" "eui"))
   (compose-mail))

; este va sin compose-mail!
(defun ism/mail-listas ()
   (interactive)
   (setq user-mail-address "joseleopoldo1792@gmail.com")
   (setq message-sendmail-extra-arguments '("-a" "joseleopoldo1792")))

(defun fcc-hinzu ()
  "Configuraciones al enviar un email."
  (interactive)
  (message-goto-fcc)
  (insert "~/sent-items"))

(defun ism/email-general ()
  "Configuraciones al enviar un email."
  (interactive)
  (message-goto-from)
  (let (p1 p2 myLine)
    (setq p1 (line-beginning-position) )
    (setq p2 (line-end-position) )
    (setq myLine (buffer-substring-no-properties p1 p2))
    (setq nombre (substring myLine -20 -1))
; y ahora el if...
    (cond ((equal nombre "igor.sosa@gmail.com")
	(progn
	  (message-goto-fcc)
	  (insert "~/sent-items")
	  ))
	  ((equal nombre "Igor.Sosa@eui.emea.microsoftonline.com")
	   (progn
	     (message-goto-fcc)
	     (insert "~/sent-items-eui")
	     (message-goto-signature)
	     (insert "\n--\n")
	     (insert-file "~/.mutt/signature")
	     (message-goto-reply-to)
	     (insert "Igor.Sosa@eui.eu")
	     ))
	  ((equal nombre "joseleopoldo1792@gmail.com")
	   (progn
	     (message-goto-fcc)
	     (insert "~/sent-items")
	     ))
      )))


;
; con esto salta a la agenda si no se hace nada en 10 mins
;

(defun jump-to-org-agenda ()
  (interactive)
  (let ((buf (get-buffer "*Org Agenda*"))
        wind)
    (if buf
        (if (setq wind (get-buffer-window buf))
            (select-window wind)
          (if (called-interactively-p)
              (progn
                (select-window (display-buffer buf t t))
                (org-fit-window-to-buffer)
                ;; (org-agenda-redo)
                )
            (with-selected-window (display-buffer buf)
              (org-fit-window-to-buffer)
              ;; (org-agenda-redo)
              )))
      (call-interactively 'org-agenda-list)))
  ;;(let ((buf (get-buffer "*Calendar*")))
  ;;  (unless (get-buffer-window buf)
  ;;    (org-agenda-goto-calendar)))
  )


; disable vc for Org mode agenda files (David Maus)
;Even if you use Git to track your agenda files you might not need vc-mode to be enabled for these files
(add-hook 'find-file-hook 'dmj/disable-vc-for-agenda-files-hook)
(defun dmj/disable-vc-for-agenda-files-hook ()
  "Disable vc-mode for Org agenda files."
  (if (and (fboundp 'org-agenda-file-p)
           (org-agenda-file-p (buffer-file-name)))
      (remove-hook 'find-file-hook 'vc-find-file-hook)
    (add-hook 'find-file-hook 'vc-find-file-hook)))


;;----------------------------------------------------
;; Cosas de clock
;; tomadas de: AÑADIR PAG!!
;;----------------------------------------------------

(defun bh/clock-in ()
  (interactive)
  (setq bh/keep-clock-running t)
  (org-agenda nil "C"))

(defun bh/clock-out ()
  (interactive)
  (setq bh/keep-clock-running nil)
  (when (org-clock-is-active)
    (org-clock-out)))

(defun bh/clock-in-default-task ()
  (save-excursion
    (org-with-point-at org-clock-default-task
      (org-clock-in))))

(defun bh/clock-out-maybe ()
  (when (and bh/keep-clock-running (not org-clock-clocking-in) (marker-buffer org-clock-default-task))
    (bh/clock-in-default-task)))

;(add-hook 'org-clock-out-hook 'bh/clock-out-maybe 'append)

(defun bh/clock-in-last-task ()
  "Clock in the interrupted task if there is one"
  (interactive)
  (let ((clock-in-to-task (if (org-clock-is-active)
                              (setq clock-in-to-task (cadr org-clock-history))
                            (setq clock-in-to-task (car org-clock-history)))))
    (org-with-point-at clock-in-to-task
      (org-clock-in nil))))

;; Remove empty CLOCK drawers on clock out
(defun bh/remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line 0)
    (org-remove-empty-drawer-at "CLOCK" (point))))

;(add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)


; con esto guardamos en una variable el buffer fundamental
(setq ism/buffer-trabajante nil)

(defun ism/ir-a-buffer-trabajante ()
  "Ir al buffer principal de trabajo"
  (interactive)
  ; la función buffer-exists la defino arriba
  (if (boundp 'ism/nombre-del-buffer)
      (switch-to-buffer ism/buffer-trabajante)))

(defun ism/definir-buffer-trabajante ()
  "Definir el  buffer principal de trabajo"
  (interactive)
  ; la función buffer-exists la defino arriba
  (setq ism/buffer-trabajante (buffer-name)))

(defalias 'idb 'ism/definir-buffer-trabajante)

;; (defun ism/cambiar-diccionario ()
;;   "Es un toggle que cambia el diccionario de alemán a español y viceversa."
;;   (interactive)
;; ; tal vez se puede utilizar un tal ispell-alternate?
;;   (if (equal ispell-current-dictionary "castellano8")
;;       (ispell-change-dictionary "deutsch8")
;;     (ispell-change-dictionary "castellano8")))

;; (defalias 'od 'ism/cambiar-diccionario) ; od= otro diccionario

(defalias 'fso 'flyspell-mode-off)

(defconst tareas-reloj '(
			 ("libro-tesis" . "40a8f769-eb52-4a30-b218-1eeda3b63fb3")
			 ("proyectonacional" . "ed7c0884-a7d4-493b-b2bf-71c3f4dcc011")
			 ("ehumanista" . "17165c40-f6ae-4259-9f5f-a369f6884f75")
			 ("ddr" . "3a8c89e9-5e91-4d2c-ab40-1f268719a512")
			 
))

(defun ism/fichar-en-tarea ()
  "Permite escoger cuál es la tarea básica."
  (interactive)
  (setq bh/keep-clock-running t)
  (setq tareas-lista (mapcar 'car tareas-reloj))
  (setq tarea (ido-completing-read "¿Qué tarea? " tareas-lista))
  (setq codigo (cdr (assoc tarea tareas-reloj)))
  ; y luego el código para saltar a donde queremos
  (save-restriction
    (widen)
    (org-with-point-at (org-id-find codigo 'marker)
      (progn (org-clock-mark-default-task)
	     (org-clock-in nil)))))


(defalias 'ipi 'ism/punch-in)

(defun ism/archivar ()
  (interactive)
  ;; en lugar de 'file se puede poner 'agenda para que coja todos
  ;; los archivos de la agenda
  (org-map-entries 'org-archive-subtree "/HECHO" 'file))

(defun ism/saltar-a-punto (tarea)
  "Permite escoger cuál es la tarea básica."
  (interactive "s¿Dónde seguir? ")
  (cond ((equal tarea "l") ; limosna
	 ;; no entiendo por qué el widen va antes del org-id-goto
	 ;; pues en teoría debería ir antes al punto y luego ampliar, más que nada porque así
	 ;; estaría en el buffer apropiado, pero solamente funciona así con el widen antes...
	 (progn (widen)
		(org-id-goto  "8f263091-b79b-4c3d-8288-c4610f784322") 
		(org-narrow-to-subtree)))
	((equal tarea "ch") ; chapt hacienda
	 (progn (widen)
		(org-id-goto  "b8c5b1c5-a6ae-40c2-a662-a8e73835e362") 
		(org-narrow-to-subtree)))
 ))

(defun ism/conectar-jabber ()
  "Función para conectar jabber. La pongo porque el jabber-connect me conecta como offline no sé por qué."
  (interactive)
  (jabber-connect :username "igor.sosa@gmail.com" :network-server "talk.google.com" :connection-type 'ssl)
;  (jabber-send-presence "dnd" jabber-default-status)
)

(defalias 'cj 'jabber-connect)
(defalias 'dj 'jabber-disconnect)

(defalias 'gu 'gnus-unplugged)

;; (defun ism/correo-privado ()
;;   "Abrir vm en la carpeta de correo privado."
;;   (interactive)
;;   (vm-visit-imap-folder propio:nuevo))

(defun ism/arrancar-erc ()
  "Arrancar ERC"
  (interactive)
  (erc :server "irc.freenode.net" :port 6667 :nick "rogorido" :full-name "Igor Sosa Mayor" :password "freenodeando")
;  (erc :server "irc.oftc.net" :port 6667 :nick "rogorido" :full-name "Igor Sosa Mayor")
;  (erc-track-mode -1) ; para que ponga en la modeline;
		      ; sorprendentemetne si se pone nil lo activa...
)

(defun ism/a-o-svenska ()
  "Schwedisches å hinzufügen"
  (interactive)
  (insert "å")
)


; Funciones de mu4e

(defun ism/mu4e-personal ()
  "Cambiar a las direcciones personales"
  (interactive)
  (setq mu4e-reply-to-address "igor.sosa@gmail.com"
	user-full-name "Igor Sosa Mayor"
	message-signature nil
	user-mail-address "igor.sosa@gmail.com"
	mu4e-sent-folder "/sent")
  (message "Configuración personal de email cargada."))

(defun ism/mu4e-eui ()
  "Cambiar a la dirección el EUI"
  (interactive)
  (setq mu4e-reply-to-address "Igor.Sosa@eui.eu"
	user-mail-address "Igor.Sosa@eui.eu"
	user-full-name "Igor Sosa Mayor"
	message-signature t
	message-signature-file "/home/igor/.mutt/signature"
	mu4e-sent-folder "/eui-gesendet")
  (message "Configuración de email del EUI cargada."))

(defun ism/mu4e-erfurt ()
  "Cambiar a la dirección el Erfurt"
  (interactive)
  (setq mu4e-reply-to-address "igor.sosa_mayor@uni-erfurt.de"
	user-mail-address "igor.sosa_mayor@uni-erfurt.de"
	user-full-name "Igor Sosa Mayor"
	message-signature t
	message-signature-file "/home/igor/.mutt/signature-erfurt"
	mu4e-sent-folder "/erfurt/Sent")
  (message "Configuración de email de Erfurt cargada."))

;; (defun mu4e-change-user-based-on-maildir ()
;;   "Cambiar dependiendo del maildir donde estamos."
;;   (cond
;;    ((equal mu4e~headers-last-query "\"maildir:/inbox\"")
;;     (ism/mu4e-personal))
;;    ((equal mu4e~headers-last-query "\"maildir:/erfurt/studenten\"")
;;     (ism/mu4e-erfurt))
;;    ((equal mu4e~headers-last-query "\"maildir:/eui\"")
;;     (ism/mu4e-eui))))

;--------------------------------
; Alarma
;---------------------------------

(defvar alarm-clock-timer nil
  "Keep timer so that the user can cancel the alarm")

(defun alarm-clock-message (text)
  "The actual alarm action"
  (progn
    (let((i 0))
      (while (< i 1) ; estaba puesto en 3
        (play-sound-file "~/Documents/gallo.wav")
        (setq i (1+ i)))
      (message-box text))))

(defun alarm-clock ()
  "Set an alarm.
The time format is the same accepted by `run-at-time'.  For
example \"11:30am\"."
  (interactive)
  (let ((time (read-string "Time: "))
        (text (read-string "Alarm message: ")))
    (progn
      (setq alarm-clock-timer 
          (run-at-time time nil 'alarm-clock-message text)))))

(defun alarm-clock-cancel ()
  "Cancel the alarm clock"
  (interactive)
  (cancel-timer alarm-clock-timer))

; emails

(defun ism/cambiar-status-jabber (mensaje)
  "Cambiar el mensaje en jabber y poner el status en dnd."
  (interactive "sMensaje: ")
  (jabber-send-presence "dnd" mensaje "10")
  )

(defun ism/cambiar-status-jabber-a-weg ()
  "Cambiar el mensaje en jabber y poner el status en weg."
  (interactive)
  (jabber-send-presence "away" "" "10")
  )

(defalias 'cambiar-status-jabber 'ism/cambiar-status-jabber)
(defalias 'jabber-weg 'ism/cambiar-status-jabber-a-weg)

(defalias 'by 'boxquote-yank)

(defconst contactos '(("nat" . "Nathalie Wergles <nathalie.wergles@gmail.com>")
		      ("duo" . "David Sosa Mayor <davidsosa@qq.com>, María Dolores Mayor Bernaola <doloremb@gmail.com>")
		      ("poncio" . "David Sosa Mayor <davidsosa@qq.com>")
		      ("mama" . "María Dolores Mayor Bernaola <doloremb@gmail.com>")
		      ("pap" .  "Francisco Sosa Wagner <fsosawagner@yahoo.es>")
		      ("rod" . "Rodrigo Rodríguez Torres <rodrigo1972@gmail.com>")
		      ("jorge" . "Jorge del Palacio Martín <jrgdelpalacio@hotmail.com>")
		      ("ignacio" . "José Ignacio Ruiz Rodríguez <jignacio.ruiz@uah.es>")
		      ("vieneses" . "Luis Lidón <llidon@efe.com>, Cecilia Salcedo Cabrera <cecisalcab@gmail.com>, Ana Cancho <annacancho@yahoo.es>, Juanma Cabrera <jummacg@yahoo.es>")
))


(defun ism/mu4e-escribir ()
  "Escoger una de las personas para el email"
  (interactive)
  (setq destinatario-lista (mapcar 'car contactos))
  (setq destinatario (ido-completing-read "Email a: " destinatario-lista))
; call-interactively es necesario porque algunas de estas funciones 
; tienen un argumento
  (setq email (cdr (assoc destinatario contactos)))
  (ism/mu4e email)
)

(defun ism/mu4e (person); &optional betreff)
  "Enviar email a Nath"
  (ism/mu4e-personal)
  (mu4e-compose-new)
  (message-goto-to)
  (insert-string person)
   (message-goto-body)
)

(defun ism/enviar-link (titulo url)
  (interactive "sTitulo: \nsURL: ")
  (setq destinatario-lista (mapcar 'car contactos))
  (setq destinatario (ido-completing-read "Email a: " destinatario-lista))
; call-interactively es necesario porque algunas de estas funciones 
; tienen un argumento
  (setq email (cdr (assoc destinatario contactos)))
  (ism/mu4e-personal)
  (mu4e-compose-new)
  (message-goto-to)
  (insert-string email)
  (message-goto-subject) (insert-string titulo)
  (message-goto-body) (insert-string url)
)

(defun ism/enviar-link-desde-gnus ()
  (interactive)
  (let (url)
    (with-current-buffer gnus-original-article-buffer
      (setq url (gnus-fetch-field "Archived-at")))
    (if (not (stringp url))
        (gnus-message 1 "No \"Archived-at\" header found.")
      (setq url (gnus-replace-in-string url "^<\\|>$" "")))
    (gnus-summary-mail-forward)
    (gnus-alias-use-identity "general")
    (message-goto-body)
    (let ((inicio (point)))
      (end-of-buffer)
      (kill-region inicio (point)))
    (insert-string url)
  (message-goto-to))
)

(defun word-count ()
  "Count words in buffer"
  (interactive)
  (shell-command-on-region (point-min) (point-max) "wc -w"))

; lo tengo como C-c f
(defun recentf-ido-find-file ()
  "Find a recent file using ido."
  (interactive)
  (let ((file (ido-completing-read "Choose recent file: " recentf-list nil t)))
    (when file
      (find-file file))))


; http://whattheemacsd.com/
;; (defun rotate-windows ()
;;   "Rotate your windows"
;;   (interactive)
;;   (cond ((not (> (count-windows)1))
;;          (message "You can't rotate a single window!"))
;;         (t
;;          (setq i 1)
;;          (setq numWindows (count-windows))
;;          (while  (< i numWindows)
;;            (let* (
;;                   (w1 (elt (window-list) i))
;;                   (w2 (elt (window-list) (+ (% i numWindows) 1)))

;;                   (b1 (window-buffer w1))
;;                   (b2 (window-buffer w2))

;;                   (s1 (window-start w1))
;;                   (s2 (window-start w2))
;;                   )
;;              (set-window-buffer w1  b2)
;;              (set-window-buffer w2 b1)
;;              (set-window-start w1 s2)
;;              (set-window-start w2 s1)
;;              (setq i (1+ i)))))))


;; (defun insert-date (prefix)
;;     "Insert the current date. With prefix-argument, use ISO format. With
;;    two prefix arguments, write out the day and month name."
;;     (interactive "P")
;;     (let ((format (cond
;;                    ((not prefix) "%d/%m/%Y")
;;                    ((equal prefix '(4)) "%Y-%m-%d")
;;                    ((equal prefix '(16)) "%A, %d. %B %Y")))
;;           (system-time-locale "de_DE"))
;;       (insert (format-time-string format))))

; cogida de dónde?
(defun duplicate-line-or-region (&optional n)
  "Duplicate current line, or region if active.
With argument N, make N copies.
With negative N, comment out original line and use the absolute value."
  (interactive "*p")
  (let ((use-region (use-region-p)))
    (save-excursion
      (let ((text (if use-region        ;Get region if active, otherwise line
                      (buffer-substring (region-beginning) (region-end))
                    (prog1 (thing-at-point 'line)
                      (end-of-line)
                      (if (< 0 (forward-line 1)) ;Go to beginning of next line, or make a new one
                          (newline))))))
        (dotimes (i (abs (or n 1)))     ;Insert N times, or once if not specified
          (insert text))))
    (if use-region nil                  ;Only if we're working with a line (not a region)
      (let ((pos (- (point) (line-beginning-position)))) ;Save column
        (if (> 0 n)                             ;Comment out original with negative arg
            (comment-region (line-beginning-position) (line-end-position)))
        (forward-line 1)
        (forward-char pos)))))

; esto no sé de dónde sale...
(defun my-bell-function ()
  (unless (memq this-command
        '(isearch-abort abort-recursive-edit exit-minibuffer
               mwheel-scroll down up next-line previous-line
              backward-char forward-char))
    (ding)))
(setq ring-bell-function 'my-bell-function)

; con esto se para el reloj si está en marcha
; importante es que hay que añadirlo al hook
(defun org-clock-out-maybe ()
   "Stop a currently running clock."
   (org-clock-out nil t)
   (org-save-all-org-buffers))

(add-hook 'kill-emacs-hook 'org-clock-out-maybe)

(defun ism/saltar-inicio-footnote ()
  (interactive)
  (goto-char (TeX-find-macro-start))
  (search-forward "{" (TeX-find-macro-end) t))

(defun ism/saltar-fin-footnote ()
  (interactive)
  (goto-char (TeX-find-macro-end))
  (backward-char)
  )

; adaptada de una función que está en
; http://www.emacswiki.org/emacs/KillingBuffers
(defun ism/eliminar-buffers-molestos ()
  "Elimina buffers molestos. Por ahora, dired, ess-help y help."
	 (interactive)
	 (mapc (lambda (buffer) 
           (when (or (eq 'ess-help-mode (buffer-local-value 'major-mode buffer))
		     (eq 'dired-mode (buffer-local-value 'major-mode buffer))
		     (eq 'magit-status-mode (buffer-local-value 'major-mode buffer))
		     (eq 'help-mode (buffer-local-value 'major-mode buffer)))
             (kill-buffer buffer))) 
         (buffer-list)))

; función para abrir un blog en firefox desde gnus
; cogido de aquí
; https://lists.gnu.org/archive/html/info-gnus-english/2012-10/msg00016.html
(defun ism/gnus-browse-archived-at ()
  "Browse \"Archived-at\" URL of the current article."
  (interactive)
  (let (url)
    (with-current-buffer gnus-original-article-buffer
      (setq url (gnus-fetch-field "Archived-at")))
    (if (not (stringp url))
        (gnus-message 1 "No \"Archived-at\" header found.")
      (setq url (gnus-replace-in-string url "^<\\|>$" ""))
      (browse-url url))))

(defun ism/gnus-browse-titulo-at ()
  "Browse \"Archived-at\" URL of the current article."
  (interactive)
  (with-current-buffer gnus-original-article-buffer
    (setq url (gnus-fetch-field "Subject"))
    (setq direccion (gnus-fetch-field "Archived-at"))
    )
    (message (concat "estamos en: " url " y la direccion es " direccion)))

(defun ism/plegar-topics ()
  (interactive)
  (mapcar  (lambda (topic)
      (gnus-topic-jump-to-topic topic)
      (if (gnus-topic-visible-p)
	  (gnus-topic-fold)))
	   '("R-stats" "blogs" "latex" "mail" "misc" "chess"))
  (gnus-topic-jump-to-topic "emacs"))

(defun ism/erc-mostrar-buffers ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (switch-to-buffer "#emacs")
  (other-window 1)
  (split-window-vertically)
  (switch-to-buffer "#emacs-es")
  (other-window 1)
  (switch-to-buffer "##esperanto")
  (balance-windows)
  ; vuelve a la window de #emacs
  (other-window 1)
)

(defalias 'ver-erc 'ism/erc-mostrar-buffers)

;;; Trucos para usar emacs como o y O en vim
;; Behave like vi's o command
(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
  (interactive "p")
  (end-of-line)
  (open-line arg)
  (next-line 1)
  (when newline-and-indent
    (indent-according-to-mode)))
(global-set-key (kbd "C-o") 'open-next-line)

;; Behave like vi's O command
(defun open-previous-line (arg)
  "Open a new line before the current one. 
     See also `newline-and-indent'."
  (interactive "p")
  (beginning-of-line)
  (open-line arg)
  (when newline-and-indent
    (indent-according-to-mode)))
(global-set-key (kbd "M-o") 'open-previous-line)
;; Autoindent open-*-lines
(defvar newline-and-indent t
  "Modify the behavior of the open-*-line functions to cause them to autoindent.")

(defalias 'mwb 'mark-whole-buffer)

(defun ism/borrar-hasta-final ()
  (interactive)
  (beginning-of-line)
  (let ((inicio (point)))
    (end-of-buffer)
    (kill-region inicio (point)))
)
(defalias 'bf 'ism/borrar-hasta-final)

(defun ism/marcar-mensajes-leidos-gmail ()
  (interactive)
  (shell-command "~/bin/imapmarcarleido.py"))

(defalias 'melg 'ism/marcar-mensajes-leidos-gmail)
(defalias 'afm 'auto-fill-mode)

(defun unfill-region (beg end)
      "Unfill the region, joining text paragraphs into a single
    logical line.  This is useful, e.g., for use with
    `visual-line-mode'."
      (interactive "*r")
      (let ((fill-column (point-max)))
        (fill-region beg end)))
    
;; Handy key definition
;(define-key global-map "\C-\M-Q" 'unfill-region)

(defun ism/ess-delete-all-results ()
  "Borra todo los resultados de R y deja solo los comandos."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (delete-non-matching-lines "^[>+]")
  )
)

(defun ism/gnus-limpiar-mensaje ()
  "Limpiar las citas de un mensaje."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (delete-matching-lines "^[>]\\{2,\\}")
  )
)


(defun ism/mu4e-cambiar-direccion-erfurt ()
  "Cambia la dirección y otras variables en una respuesta a
la dirección de Erfurt."
  (interactive)
  (save-excursion
    (message-narrow-to-headers)
    (replace-string "igor.sosa@gmail.com" "igor.sosa_mayor@uni-erfurt.de")
    (widen)
    (make-local-variable 'mu4e-sent-folder)
    (make-local-variable 'message-signature-file)
    (make-local-variable 'message-signature)
    (setq mu4e-sent-folder "/erfurt/Sent")
    (setq message-signature t)
    (setq message-signature-file "~/.mutt/signature-erfurt")
    (message-insert-signature)))

(defalias 'erfurt 'ism/mu4e-cambiar-direccion-erfurt)

(defalias 'mui 'mu4e-update-index)

; para borrar las footnotes que pone scid
(defun ism/borrar-notas-ajedrez ()
  (interactive)
  (query-replace-regexp "\\$^{[0-9]+}\\$" "")
  )

; para lo de puntos, comas que no van seguidos de espacios sería esto:
; "[.,?!][a-zA-Z]"
;; (defun ism/corregir-sin-espacio ()
;;   (interactive)
;;   (query-replace-regexp "\\([.,?!]\\)\\([a-zA-Z]\\)" "\\1 \\2")
;;   )

; lo mismo que lo anterior pero con occur, que es mejor!
; lo otro es muy agresivo!
(defun ism/occur-sin-espacio ()
  (interactive)
  (occur "[.,?!][a-zA-Z]")
  )

(defun ism/exportar-gnus-to-mutt ()
  (interactive)
  (if (fboundp 'gnus-group-exit)
      (gnus-group-exit))
  (shell-command "~/bin/gnus2mutt.sh")
  )

(defun ism/latex-reducir-vision-texto ()
  "Esta función sirve de una especie de narrow para latex."
  (interactive)
  (save-excursion
    (LaTeX-mark-section t) ; la t sirve para que NO marque subsections!
    (narrow-to-region (region-beginning) (region-end)))
  )

(defun ism/email-jesuitas (tema)
  "Enviar un email a los estudiantes del seminario de jesuitas."
  (interactive "sTema del email: ")
  (gnus-group-mail)
  (gnus-alias-use-identity "erfurt")
  (message-goto-to)
  (insert-string "aljesuiten")
  (expand-abbrev)
  (message-goto-subject)
  (insert-string tema)
  (message-goto-body)
  (insert-string "st")
  (yas-expand)
  (ism/switch-diccionario)
 )

(defun ism/email-proto (tema)
  "Enviar un email a los estudiantes del seminario de protonacionalismo."
  (interactive "sTema del email: ")
  (gnus-group-mail)
  (gnus-alias-use-identity "erfurt")
  (message-goto-to)
  (insert-string "alproto")
  (expand-abbrev)
  (message-goto-subject)
  (insert-string tema)
  (message-goto-body)
  (insert-string "st")
  (yas-expand)
  (ism/switch-diccionario)
 )


; cambiar el diccionario de completar
(defun ism/cambiar-completar (&optional lengua)
  "Cambiar el fichero de completar"
  (interactive)
  (or lengua
      (progn
      (setq idiomas '("alemán" "español"))
      (setq lengua (ido-completing-read "Cambiar a: " idiomas))))
  (if (equal lengua "español")
      (setq ac-dictionary-files '("~/.dict"))
    (setq ac-dictionary-files '("~/.aleman")))
  (ac-config-default)
  (ac-clear-dictionary-cache)
)

(defun ism/borrar-frase ()
  "Borrar una frase entera aunque estemos en media de ella.
Es decir, mejora kill-sentence (M-k)"
  (interactive)
  (backward-sentence)
  (kill-sentence)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; change case of letters                                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; http://ergoemacs.org/emacs/modernization_upcase-word.html
(defun toggle-letter-case ()
  "Toggle the letter case of current word or text selection.
Toggles between: “all lower”, “Init Caps”, “ALL CAPS”."
  (interactive)
  (let (p1 p2 (deactivate-mark nil) (case-fold-search nil))
    (if (region-active-p)
        (setq p1 (region-beginning) p2 (region-end))
      (let ((bds (bounds-of-thing-at-point 'word) ) )
        (setq p1 (car bds) p2 (cdr bds)) ) )

    (when (not (eq last-command this-command))
      (save-excursion
        (goto-char p1)
        (cond
         ((looking-at "[[:lower:]][[:lower:]]") (put this-command 'state "all lower"))
         ((looking-at "[[:upper:]][[:upper:]]") (put this-command 'state "all caps") )
         ((looking-at "[[:upper:]][[:lower:]]") (put this-command 'state "init caps") )
         ((looking-at "[[:lower:]]") (put this-command 'state "all lower"))
         ((looking-at "[[:upper:]]") (put this-command 'state "all caps") )
         (t (put this-command 'state "all lower") ) ) )
      )

    (cond
     ((string= "all lower" (get this-command 'state))
      (upcase-initials-region p1 p2) (put this-command 'state "init caps"))
     ((string= "init caps" (get this-command 'state))
      (upcase-region p1 p2) (put this-command 'state "all caps"))
     ((string= "all caps" (get this-command 'state))
      (downcase-region p1 p2) (put this-command 'state "all lower")) )
    )
  )

;;set this to M-c
(global-set-key "\M-c" 'toggle-letter-case)

; ---------------------------------------------------------
; para la tesis
; ---------------------------------------------------------
(defun ism/buscar-en-tesis (busqueda)
  "Buscar en los tex de la tesis."
  (interactive "sPatrón de búsqueda: ")
  (lgrep busqueda "*.tex" "~/geschichte/konfessiodiss/text/versionfinal"))

(defun ism/abrir-directorio-tesis ()
  "Abrir el directorio de la tesis en dired"
  (interactive)
  (dired "~/geschichte/konfessiodiss/text/versionfinal")
  )

; otra opción con helm
(defun ism/buscar-en-tesis-helm ()
  (interactive)
  (helm-do-grep-1 '("~/geschichte/konfessiodiss/text/versionfinal/*.tex")))

; realmente esto se podría tal vez hacer un poco más fácil
; cambiando con una función la variable default-directory
(defun ism/abrir-ficheros-zusammenfassungen ()
  "Abrir helm en el directorio de zusammenfassungen."
  (interactive)
  (setq dir  "~/geschichte/zusfassungen/"
	zf (directory-files dir nil "\\(org\\|tex\\)$"))
  (let ((some-helm-source
       '((name . "Zusammenfassungen")
         (candidates . zf)
         (action . (lambda (candidate)
                     (find-file (concat dir candidate)))))))
    (helm :sources '(some-helm-source)))
  )

(defun ism/abrir-ficheros-tesis ()
  "Abrir helm en el directorio de ficheros de teología moral."
  (interactive)
  (setq dir "~/geschichte/konfessiodiss/text/versionfinal/"
	zf (directory-files dir nil "tex$"))
  (let ((some-helm-source
       '((name . "Teología moral y nobles")
         (candidates . zf)
         (action . (lambda (candidate)
                     (find-file (concat dir candidate)))))))
    (helm :sources '(some-helm-source)))
  )

; ---------------------------------------------------------
; para lernen
; ---------------------------------------------------------
(defun ism/abrir-ficheros-lernen ()
  "Abrir helm en el directorio de ficheros de lernen."
  (interactive)
  (setq dir "~/Documents/lernen/"
	zf (directory-files dir nil "org$"))
  (let ((some-helm-source
       '((name . "Ficheros para lernen")
         (candidates . zf)
         (action . (lambda (candidate)
                     (find-file (concat dir candidate)))))))
    (helm :sources '(some-helm-source)))
  )

; ---------------------------------------------------------
; para configuración de emacs
; ---------------------------------------------------------
(defun ism/abrir-ficheros-config-emacs ()
  "Abrir helm en el directorio de ficheros de configuración de emacs."
  (interactive)
  (setq dir "~/.emacs.d/emacs-personal/"
	zf (directory-files dir nil "el$"))
  (let ((some-helm-source
       '((name . "Ficheros para configurar emacs")
         (candidates . zf)
         (action . (lambda (candidate)
                     (find-file (concat dir candidate)))))))
    (helm :sources '(some-helm-source)))
  )

(defun ism/iniciar-todo ()
  (interactive)
  (make-frame '((name . "gnus")))
  (make-frame '((name . "irc")))
  )

(defalias 'quitar/poner-barramenu 'toggle-menu-bar-mode-from-frame)

(defun ism/cfw-deadlines ()
  "Ver el calendario solo con los deadlines."
  (interactive)
  (setq cfw:org-agenda-schedule-args '(:deadline))
;  (setq org-agenda-files (quote ("~/Documents/org/konferenzen.org"
;				 "~/Documents/org/jobsuche.org")))
  (cfw:open-org-calendar)
  )

(defalias 'mis-deadlines 'ism/cfw-deadlines)


(provide 'ism-functions)

