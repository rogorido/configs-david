(message "Hydras werden geladen...")

(defhydra hydra-mingus (:color blue)
  "mingus"
  ("m" mingus "mingus")
  ("p" mingus-toggle "play/pause")
  ("c" mingus-clear "limpiar lista")
  ("l" mingus-load-playlist "cargar lista")
  ("r" mingus-random "random")
  ("a" mingus-prev "anterior")
  ("n" mingus-next "siguiente"))

(defhydra hydra-erc (:color blue)
  "erc"
  ("c" ism/arrancar-erc "conectar")
  ("v" ism/erc-mostrar-buffers "ver erc")
  ("b" erc-switch-to-buffer "ver erc"))

(defhydra hydra-tesis (:color blue)
  "Tesis"
  ("b" ism/buscar-en-tesis "buscar")
  ("h" ism/buscar-en-tesis-helm "buscar con helm")
  ("z" ism/abrir-ficheros-zusammenfassungen "zusammenfassungen")
  ("f" ism/abrir-ficheros-tesis "ficheros tesis")
  ("d" ism/abrir-directorio-tesis "dired"))

; esto es un poco absurdo...
(defhydra hydra-help (:color blue :columns 8)
  "Help"
  ("f" describe-function "function")
  ("F" Info-goto-emacs-command-node "goto command")
  ("v" describe-variable "variable")
  ("m" describe-mode "mode")
  ("@" describe-face "face")
  ("k" describe-key "key")
  ("t" describe-theme "theme")
  ("b" describe-bindings "bindings")
  ("p" describe-package "package"))


(defhydra hydra-general (:color blue :hint nil)
  "Acción"
  ("e" ism/abrir-ficheros-config-emacs "config emacs")
  ("l" ism/abrir-ficheros-lernen "lernen")
  ("f" ism/fichar-en-tarea "fichar")
  ("i" hydra-erc/body "ERC")
;  ("a" hydra-help/body "ayuda") ; esto es un poco absurdo
  ("m" hydra-mingus/body "mingus") 
  ("a" ism/ir-a-agenda "agenda") 
  ("d" ism/cfw-deadlines "deadlines")
  ("t" hydra-tesis/body "tesis"))

(defhydra hydra-general-blog (:color pink
                             :hint nil :exit t)
  "
^General^             ^Wordpress^        ^Tareas^         ^Otros^
^^^^^^^^--------------------------------------------------------------------
_o_: abrir general    _l_: login         _a_: ver         _b_: buscar en entradas
_n_: nueva entrada    _u_: subir draft   _ñ_: añadir      _i_: idea en bruto
^ ^                   _p_: publicar      ^ ^              _m_: modo lineas para el blog 

"
  ("l" org2blog/wp-login)
  ("n" org2blog/wp-new-entry)
  ("u" org2blog/wp-post-buffer)
  ("p" dsm/publicar-entrada-blog)
  ("o" dsm/abrir-general-blog-turquistan)
  ("b" dsm/buscar-en-blogs)
  ("a" dsm/agenda-blog)
  ("ñ" dsm/añadir-tarea-blog)
  ("i" dsm/idea-bruto-blog)
  ("m" dsm/activar-modo-lineas-blog)
  ("q" nil "cancelar"))

(defhydra hydra-general-total (:color pink
                             :hint nil :exit t)
  "
^General^              ^Agenda^          ^Calendario^            ^Ficheros Org^
^^^^^^^^-----------------------------------------------------------------------
_f_ buffers            _a_: general      _c_: calendario limpio  _o_ ordenar lista    
_y_ ayuda              _t_: trabajo      _d_: deadlines          _n_ navegar
_e_ email              _b_: blog                                 _l_ añadir link
_O_ olivetti           _c_: calendario                           _r_ reducir a subtree
_m_: marcar línea      ^ ^                                       _w_ mostrar todo
"
  ("f" ibuffer)
  ("y" dsm/abrir-fichero-ayuda)
  ("o" dsm/abrir-general-blog-turquistan)
  ("b" dsm/buscar-en-blogs)
  ("e" mu4e)
  ("a" org-agenda)
  ("c" cfw:open-calendar-buffer)
  ("d" ism/cfw-deadlines)
  ("t" revert-buffer)
  ("n" helm-imenu)
  ("m" global-hl-line-mode)
  ("r" org-narrow-to-subtree)
  ("w" widen)
  ("o" org-sort-list)
  ("l" org-insert-link)
  ("O" olivetti-mode)
  ("q" nil "cancel"))

;; Recommended binding:
;; (define-key Buffer-menu-mode-map "." 'hydra-buffer-menu/body)
