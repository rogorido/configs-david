(message "Cargando el cheatsheet...")

(add-to-list 'load-path "~/packages-emacs/cheatsheet")

(require 'cheatsheet)

; parece que hay que meterlos al revés!!

(cheatsheet-add :group 'Agenda
                :key "v m"
                :description "Vista de un mes")

(cheatsheet-add :group 'Agenda
                :key "v w"
                :description "Vista de una semana")

(cheatsheet-add :group 'Agenda
                :key "v d"
                :description "Vista de un día")

(cheatsheet-add :group 'Agenda
                :key "t"
                :description "Cambiar el TODO")




