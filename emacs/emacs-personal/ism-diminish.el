(require 'diminish)

(eval-after-load "undo-tree"
  '(diminish 'undo-tree-mode "U"))

;(eval-after-load "projectile"
;  '(diminish 'projectile-mode))

(eval-after-load "yas-minor-mode"
  '(diminish 'yas-minor-mode))

(eval-after-load "paredit"
  '(diminish 'paredit-mode))

(eval-after-load "auto-complete"
  '(diminish 'auto-complete-mode "AC"))

(eval-after-load "yasnippet"
  '(diminish 'yas-minor-mode))

;(eval-after-load "flyspell"
;  '(diminish 'flyspell-mode))

;(eval-after-load "workgroups"
;  '(diminish 'workgroups-mode))

(eval-after-load "eldoc"
  '(diminish 'eldoc-mode))

(eval-after-load "helm"
  '(diminish 'helm-mode))

(diminish 'abbrev-mode "Ab")
;(diminish 'lisp-mode "el")

(provide 'ism-diminish)
