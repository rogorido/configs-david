(message "Cargando las configuraciones de Orgmode...")

; enteindo que esto es necesario solo con el paquete de AUR
;(require 'org-install)
(require 'org)
;(require 'org-contacts)
; parece ser que esto sirve para que se carguen
; los autoloads, aunque creo que no es del todo necesario
;(require 'org-loaddefs.el)

(setq org-agenda-inhibit-startup t)

(add-hook 'org-mode-hook '(lambda ()
			    (setq default-justification 'left)))

; con esto utilizo orgmode también para archivos txt
(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;(global-set-key "\C-ci" 'bibtex-insertar)

; en esto pongo todos los importante para que luego
; salga en lo de deadlines; la agenda del día tiene una
; lista reducida
(setq org-agenda-files (quote ("~/Escritorio/general/org/privado.org"
;			       "~/Escritorio/general/org/notes.org"
			       "~/Escritorio/general/org/proyectos.org"
			       "~/Escritorio/general/org/estudiar.org"
			       "~/Escritorio/general/org/buscar_trabajo.org")))

;(setq org-contacts-files '("~/Documents/org/contacts.org"))

(setq org-refile-targets '((org-agenda-files :maxlevel . 1)
			   (nil :maxlevel . 1)))

; Targets start with the file name - allows creating level 1 tasks
;(setq org-refile-use-outline-path (quote file))
; lo cambio por esto... entiendo que funciona mejor con ido
(setq org-refile-use-outline-path t)

; Targets complete in steps so we start with filename, TAB shows the next level of targets etc
; con ido no es recomendable
(setq org-outline-path-complete-in-steps nil)

; con esto es posible usar las habilidades de orgmode para estructurar
; texto también cuando escribo emails...
;(add-hook 'message-mode-hook 'orgstruct++-mode 'append)
;(add-hook 'message-mode-hook 'orgtbl-mode 'append)
;(add-hook 'mail-mode-hook 'orgstruct++-mode 'append)
;(add-hook 'mail-mode-hook 'orgtbl-mode 'append)

;(require 'org-habit)
; carga un módulo que es como el toc de latex
;(require 'org-toc)
;(require 'org-drill) ; lo cargo abajo en la nueva versión

;(add-to-list 'org-modules 'org-habit)
;(setq org-habit-show-habits 'nil)

(setq org-confirm-babel-evaluate nil)

(setq org-enforce-todo-dependencies t)

(setq org-archive-location "archive/%s_archive::")

(setq org-agenda-span 'day
      org-agenda-show-all-dates t
      org-agenda-skip-deadline-if-done t
      org-agenda-skip-scheduled-if-done t
      org-agenda-start-on-weekday nil
      org-agenda-tags-column 120
      org-deadline-warning-days 75
      org-reverse-note-order t
;(setq org-clock-in-switch-to-state "INPROGRESS")
      org-agenda-use-time-grid nil)

;This is a special interface to select and deselect tags with single keys
(setq org-use-fast-tag-selection t)

; entiendo que con esto es posible RETURN en un link
(setq org-return-follows-link t)

; que no las muestre; luego se puede poner:
; #+STARTUP: inlineimages
(setq org-startup-with-inline-images nil)

; con C-c C-o en un link del tipo mailto:
; se abre el editor de gnus
;(setq org-link-mailto-program '(gnus-msg-mail "%a" "%s"))

;(org-remember-insinuate)
(setq org-directory "~/Escritorio/general/org/")
(setq org-default-notes-file (concat org-directory "/notes.org"))
(define-key global-map "\C-cr" 'org-capture)

; agenda
(setq org-agenda-tags-todo-honor-ignore-options t
      org-agenda-follow-indirect t
      org-agenda-follow-mode t
      org-agenda-show-outline-path nil)
(load "ism-org-agenda.el")

; cargar los templates de capture
(load "ism-org-capture.el")

; encontrado en lo d las variables
(setq org-agenda-window-setup (quote current-window))
;(setq org-agenda-window-setup 'other-window)

; lo tengo worg...
(setq org-agenda-restore-windows-after-quit t)

; con esto añade el tiempo en que se ha completado una tarea
(setq org-log-done 'time)

; esto es para que 'meta' el texto después de los enunciados
(setq org-startup-indented t)

; con esto resalta la línea de la agenda en la que estamos
(add-hook 'org-agenda-mode-hook (lambda ()
				  (hl-line-mode 1)))

(setq org-cycle-separator-lines 1)

(setq org-use-speed-commands t)
(setq org-special-ctrl-a/e t)

(add-to-list 'org-speed-commands-user '("x" org-todo "HECHO"))
;(add-to-list 'org-speed-commands-user '("s" call-interactively 'org-schedule))
;(add-to-list 'org-speed-commands-user '("i" call-interactively 'org-clock-in))
;(add-to-list 'org-speed-commands-user '("o" call-interactively 'org-clock-out))

; para deshabilitar las teclas de prioridades:
(setq org-enable-priority-commands t)

; tal vez esto... quitarlo?
;; (setq org-link-frame-setup (quote ((gnus . gnus-other-frame)
;;                                    (file . find-file-other-frame))))

; entiendo que esto permite pulsar C-S-RET para añadir un nuevo heading
; y lo hace posible sin ir al final del bloque, es decir, en medio de lo
; que se está escribiendo...
(setq org-insert-heading-respect-content t)

; todo workflow
(setq org-todo-keywords
           '((sequence "TODO(t)" "|" "ESPERANDO(e)" "POSPUESTO(p)" "CANCELADO(c)" "HECHO(h)")
             (sequence "LEER(l)" "|" "LEíDO(o)")
	; para fuentes
        ;     (sequence "MIRAR(m)"  "|" "NO-ENCONTRADO(N)" "DESCARTADO(d)" "RESUMIDO(r)" "DESCARGADO(G)")
        ;     (sequence "METER(.)"  "|" "METIDO(-)")
	     ))

; esto en parte lo pongo por el tema solarized
(setq org-todo-keyword-faces
           '(("TODO" . org-warning)
	     ("LEER" . org-warning)
	     ("METER" . org-warning)
	     ("MIRAR" . org-warning)
	     ("INPROGRESS" . (:foreground "blue" :weight bold))
             ("ESPERANDO" . (:foreground "blue" :weight bold))))

; tags
(setq org-tag-alist '(("email" . ?e)
		      ("tiempolibre" . ?l)
		      ("papeleos" . ?o)
		      ("viaje" . ?v)
		      ("personal" . ?p)
		      ("biblio" . ?b)))

; increíble: con esto se consigue que en el modo agenda
; el ratón no 'ilumine' la línea sobre la que está...
(add-hook 'org-finalize-agenda-hook
    (lambda () (remove-text-properties
               (point-min) (point-max) '(mouse-face t))))


(org-clock-persistence-insinuate)
(setq org-clock-idle-time 15
      org-clock-out-remove-zero-time-clocks t
      org-clock-persist 'history
      org-drawers '("PROPERTIES" "LOGBOOK" "CLOCK")
      org-clock-into-drawer "CLOCK"
      org-log-into-drawer "LOGBOOK")
; global Effort estimate values
(setq org-global-properties (quote (("Effort_ALL" . "0:05 0:10 0:20 0:30 1:00 2:00 3:00 4:00 5:00 6:00"))))
(setq org-columns-default-format "%50ITEM(Task) %10Effort(EE){:} %CLOCKSUM %TODO %TAGS")

;; Yes it's long... but more is better ;)
(setq org-clock-history-length 512)

;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)

;; Include current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)

;; Agenda log mode items to display (clock time only by default)
(setq org-agenda-log-mode-items (quote (clock)))

; esto es la variable booleana que utiliza Hansen para 
; saber si hay que mantener el reloj corriendo o no cuando se 
; cierra una tarea
(setq bh/keep-clock-running nil)

(setq org-refile-use-cache t)

; parece que esto 'abre' un link
; al parecer es mejor no usarlo
;(setq org-tab-follows-link t)

; increíble: con esto se consigue que la agenda no salte al día
; siguiente a partir de las 12. En este caso definimos todavía 2 horas...
(setq org-extend-today-until 2)

; con esto se consigue que si una entrada tiene varios timemstamps
; los muestre todos no sólo el primero. Lo he usado por ejemplo para un partido
; del eurobasket que se juega 2 veces (pero no funciona...)
(setq org-agenda-skip-additional-timestamps-same-entry nil)

;; fontify code in code blocks
(setq org-src-fontify-natively t
      org-src-tab-acts-natively t)

;; (defun org-mode-reftex-setup ()
;;   (load-library "reftex")
;;   (and (buffer-file-name)
;;        (file-exists-p (buffer-file-name))
;;        (reftex-parse-all))
;;   (define-key org-mode-map (kbd "C-c )") 'reftex-citation))
;; (add-hook 'org-mode-hook 'org-mode-reftex-setup)

;; (setq reftex-default-bibliography
;;       (quote
;;        ("/home/igor/Documents/bibliogeneral.bib")))

(require 'ox-odt)
(require 'ox-html)
(require 'ox-latex)
;(require 'ox-beamer)
(require 'ox-md)
;(require 'ox-gfm) ; esto es markdown flavor github

; footnotes plain = [1]
(setq org-footnote-auto-label 'plain)

;(add-hook 'org-mode-hook
;                    (lambda () (imenu-add-to-menubar "Imenu")))

;; (setq org-speed-commands-user (quote (("s" . org-save-all-org-buffers)
;;                                       ("w" . org-refile)
;;                                       ("x" . ignore)
;;                                       ("y" . ignore)
;;                                       ("z" . org-add-note))))

; en teoría esto sirve para que ponga en cursiva cuando pasa de
; dos líneas, aunque no parece funcionar bien...
(setq org-emphasis-regexp-components
      '(" \t('\"{" "- \t.,:!?;'\")}\\" " \t\r\n,\"'" "." 4))


;; The following setting prevents accidentally editing hidden text
;; when the point is inside a folded region. This can happen if you
;; are in the body of a heading and globally fold the org-file with
;; =S-TAB=
(setq org-catch-invisible-edits 'error)
(setq org-export-with-smart-quotes t)

(setq org-latex-default-class "koma-artikel")

(add-to-list 'org-latex-classes
          '("koma-artikel"
             "\\documentclass[paper=a4,fontsize=12pt,DIV=11]{scrartcl}\n\\usepackage{libertine}"

             ("\\section{%s}" . "\\section*{%s}")
             ("\\subsection{%s}" . "\\subsection*{%s}")
             ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
             ("\\paragraph{%s}" . "\\paragraph*{%s}")
             ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


(provide 'ism-org)
