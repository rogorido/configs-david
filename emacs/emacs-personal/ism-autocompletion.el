(message "Cargando configuración de autocompletado...")

;(add-to-list 'load-path "/usr/share/emacs/site-lisp/auto-complete")
(require 'auto-complete-config)
;(add-to-list 'ac-dictionary-directories "/usr/share/emacs/site-lisp/auto-complete/ac-dict")
; originalmente ponemos solo la lista española
(setq ac-dictionary-files '("~/.dict"))
(ac-config-default)

(define-key ac-complete-mode-map [tab] 'ac-expand)
(define-key ac-complete-mode-map [return] 'ac-complete)

;; (setq-default
;;  ac-sources '(
;; 	      ac-source-words-in-all-buffer
;; 	      ac-source-words-in-buffer
;; 	      ))

; modos
(dolist (mode '(magit-log-edit-mode
                log-edit-mode org-mode text-mode mu4e-compose-mode
                git-commit-mode message-mode mail-mode latex-mode
                html-mode nxml-mode sh-mode jabber-chat-mode
                lisp-mode markdown-mode css-mode sql-mode
                sql-interactive-mode inferior-ess-mode
                inferior-emacs-lisp-mode erc-mode))
  (add-to-list 'ac-modes mode))

(setq-default ac-auto-start 4)
(ac-flyspell-workaround)
;(set-face-background 'ac-candidate-face "#366060")
;(set-face-background 'ac-selection-face "#8cd0d3")
;(set-face-foreground 'ac-selection-face "#1f1f1f")

(provide 'ism-autocompletion)
