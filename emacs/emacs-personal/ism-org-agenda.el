; comando más generales
(setq org-agenda-custom-commands 
'(
  ("h" "Agenda general" agenda ""
   ((org-agenda-span 1)
    (org-agenda-files '("~/Escritorio/general/org/privado.org" "~/Escritorio/general/org/buscar_trabajo.org"
			"~/Escritorio/general/org/proyectos.org" "~/Escritorio/general/org/estudiar.org")) 
    (org-deadline-warning-days 0)
    (org-deadline-warning-days 31)
    (org-agenda-sorting-strategy '(time-up todo-state-up))))
  
  ;Una lista solo con deadlines en el próximo año
  ("d" "Próximos deadlines" agenda ""
   ((org-agenda-time-grid nil)
    (org-deadline-warning-days 365)
    (org-agenda-sorting-strategy '(deadline-up))
    (org-agenda-entry-types '(:deadline))))
  ("D" "Deadlines profesionales" agenda ""
   ((org-agenda-time-grid nil)
    (org-agenda-span 'year)
    (org-agenda-files '("~/Escritorio/general/org/buscar_trabajo.org"))
    (org-agenda-sorting-strategy '(deadline-up))
    (org-agenda-entry-types '(:timestamp))))

   ; para que muestre citas pero sin schedules ni deadlines...
  ("c" . "Citas")
  ("ca" "Todas" agenda ""
   ((org-agenda-span 'week)          ;; agenda will start in week view
    (org-agenda-repeating-timestamp-show-all t)   ;; ensures that repeating events appear on all relevant dates
    (org-agenda-files '("~/Escritorio/general/org/privado.org"
			"~/Escritorio/general/org/buscar_trabajo.org"))
    (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))))
  ("ce" "Profesional (semanal)" agenda "trabajo"
   ((org-agenda-span 'week)          ;; agenda will start in week view
    (org-agenda-repeating-timestamp-show-all t)   ;; ensures that repeating events appear on all relevant dates
;    (org-agenda-files '("~/Escritorio/general/org/erfurt.org"))
    (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))))
  ("cp" "Privadas (semanal)" agenda "privado"
   ((org-agenda-span 'week)          ;; agenda will start in week view
    (org-agenda-repeating-timestamp-show-all t)   ;; ensures that repeating events appear on all relevant dates
    (org-agenda-files '("~/Escritorio/general/org/privado.org"))
    (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))))

;nicht scheduled
  ("b" . "Blogs")
  ("bt" . "Turquistán")
  ("btt" "Todo (con TODOs)" todo ""
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org"))
    (org-agenda-todo-ignore-scheduled t)))
  ("btm" "Por temas (general)" 
   (
    (tags "+uzbekistán" ((org-agenda-overriding-header "Uzbekistán")))
    (tags "+estambul" ((org-agenda-overriding-header "Estambul")))
    (tags "+lenguas" ((org-agenda-overriding-header "Lenguas")))
    (tags "+asiacentral" ((org-agenda-overriding-header "Asia central")))
    (tags "+balcanes" ((org-agenda-overriding-header "Balcanes")))
    (tags "+lenguaturca" ((org-agenda-overriding-header "Lengua turca")))
    (tags "+turquía" ((org-agenda-overriding-header "Turquía"))))
  ((org-agenda-files '("~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org"))))
  ("bto" "Por temas (sólo TODOs)" 
   ((tags-todo "+uzbekistán/-CANCELADO" ((org-agenda-overriding-header "Uzbekistán")))
    (tags-todo "+estambul/-CANCELADO" ((org-agenda-overriding-header "Estambul")))
    (tags-todo "+lenguas/-CANCELADO" ((org-agenda-overriding-header "Lenguas")))
    (tags-todo "+asiacentral/-CANCELADO" ((org-agenda-overriding-header "Asia central")))
    (tags-todo "+balcanes/-CANCELADO" ((org-agenda-overriding-header "Balcanes")))
    (tags-todo "+lenguaturca/-CANCELADO" ((org-agenda-overriding-header "Lengua turca")))
    (tags-todo "+turquía/-CANCELADO" ((org-agenda-overriding-header "Turquía"))))
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org"))))
  
  ("bte" "Por entradas"
   ((todo "CATEGORY=\"Prizren\"" ((org-agenda-overriding-header "Prizren")))
    (todo "CATEGORY=\"Prizren\"" ((org-agenda-overriding-header "Prizren"))))
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-turquistan/blog-general-turquistan.org"))))
  
  ("bc" . "Ciudad de Piedra")
  ("bct" "Todo (con TODOs)" todo ""
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-piedra/blog-general-ciudad.org"))
    (org-agenda-todo-ignore-scheduled t)))
  ("bcm" "Por temas"
   ((tags-todo "+uzbekistán/-CANCELADO" ((org-agenda-overriding-header "Uzbekistán")))
    (tags-todo "+estambul/-CANCELADO" ((org-agenda-overriding-header "Estambul")))
    (tags-todo "+lenguas/-CANCELADO" ((org-agenda-overriding-header "Lenguas")))
    (tags-todo "+asiacentral/-CANCELADO" ((org-agenda-overriding-header "Asia central")))
    (tags-todo "+balcanes/-CANCELADO" ((org-agenda-overriding-header "Balcanes")))
    (tags-todo "+lenguaturca/-CANCELADO" ((org-agenda-overriding-header "Lengua turca")))
    (tags-todo "+turquía/-CANCELADO" ((org-agenda-overriding-header "Turquía"))))
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-piedra/blog-general-ciudad.org"))))
  ("bce" "Por entradas"
   ((todo "CATEGORY=\"Prizren\"" ((org-agenda-overriding-header "Prizren")))
    (todo "CATEGORY=\"Prizren\"" ((org-agenda-overriding-header "Prizren"))))
   ((org-agenda-files '("~/Escritorio/general/blogs/blog-piedra/blog-general-ciudad.org"))))

; para archivar
  ("A" "Archivar" todo "HECHO|LEíDO|METIDO"
   ((org-agenda-overriding-header "Tasks to Archive")))

; pruebas
  ;; ("G" "GTD Block Agenda"
  ;;  ((tags "+diario" ((org-agenda-files '("~/Documents/org/gtd.org"))))
  ;;  (tags "+semanal"  ((org-agenda-files '("~/Documents/org/gtd.org"))))))
  ))

(setq org-agenda-custom-commands 
(append org-agenda-custom-commands 
'(
; zum lesen
  ("l" . "Zum Lesen")
  ("l1" "Alles" tags-todo "/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zum Lesen...")))
  ("l2" "Moral" tags-todo "+moral/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Moral...")))
  ("l3" "Politik" tags-todo "+politik/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Politik...")))
  ("l4" "Soziologie" tags-todo "+soziologie/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Soziologie...")))
  ("l5" "Computer" tags-todo "+computer/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Informatik...")))
  ("l6" "Wirtschaft" tags-todo "+wirtschaft/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Wirtschaft...")))
  ("l7" "Religion" tags-todo "+religion/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Religion...")))
  ("l8" "Schach" tags-todo "+ajedrez/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Schach...")))
  ("l9" "Statistik" tags-todo "+statistik/LEER"
   ((org-agenda-files '("~/Documents/org/lernen.org" "~/Documents/org/privat.org"))
    (org-agenda-todo-ignore-with-date nil)
    (org-agenda-todo-ignore-deadlines nil)
    (org-agenda-todo-ignore-scheduled nil)
    (org-agenda-overriding-header "Sachen zu Schach...")))
)))

; modelo para seminarios: 
;; (setq org-agenda-custom-commands 
;; (append org-agenda-custom-commands 
;; '(("1" . "Raum-Zeit")
;;   ("1ñ" "Unscheduled thesis"  
;;    ((tags-todo "+organizacion/-CANCELADO" ((org-agenda-overriding-header "Organización...")))
;;     (tags-todo "+email/-CANCELADO" ((org-agenda-overriding-header "Emails...")))
;;     (tags-todo "+sitzung3/-CANCELADO" ((org-agenda-overriding-header "Sitzung 3...")))
;;     (tags-todo "+sitzung4/-CANCELADO" ((org-agenda-overriding-header "Sitzung 4...")))
;;     (tags-todo "+sitzung5/-CANCELADO" ((org-agenda-overriding-header "Sitzung 5...")))
;;     (tags-todo "+sitzung6/-CANCELADO" ((org-agenda-overriding-header "Sitzung 6...")))
;;     (tags-todo "+sitzung7/-CANCELADO" ((org-agenda-overriding-header "Sitzung 7...")))
;;     (tags-todo "+sitzung8/-CANCELADO" ((org-agenda-overriding-header "Sitzung 8...")))
;;     (tags-todo "+sitzung9/-CANCELADO" ((org-agenda-overriding-header "Sitzung 9...")))
;;     (tags-todo "+sitzung10/-CANCELADO" ((org-agenda-overriding-header "Sitzung 10...")))
;;     (tags-todo "+sitzung11/-CANCELADO" ((org-agenda-overriding-header "Sitzung 11...")))
;;     (tags-todo "+sitzung12/-CANCELADO" ((org-agenda-overriding-header "Sitzung 12...")))
;;     (tags-todo "+sitzung13/-CANCELADO" ((org-agenda-overriding-header "Sitzung 13...")))
;;     (tags-todo "+sitzung14/-CANCELADO" ((org-agenda-overriding-header "Sitzung 14...")))
;;     (tags-todo "+sitzung15/-CANCELADO" ((org-agenda-overriding-header "Sitzung 15..."))))
;; ((org-agenda-files '("~/geschichte/seminarios/raum-ws/raum-ws.org"))))
;; )))

