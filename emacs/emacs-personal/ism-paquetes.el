(message "Cargando la configuración de gestión de paquetes...")

(require 'package)
;; Add the original Emacs Lisp Package Archive
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
;(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
;                         ("marmalade" . "https://marmalade-repo.org/packages/")
;                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)
; esto no sé qué es...
;(when (not package-archive-contents)
;  (package-refresh-contents))
(provide 'ism-paquetes)
