#!/bin/bash

usage()
{
cat << EOF
uso: $0 nombrefichero

Este programa sirve para emitir videos desde VLC. Hace falta poner
el nombre del fichero entre comillas.

EOF
}

if [[ $# -gt 1 ]]
then
    usage
    exit 1
elif [[ $# -lt 1 ]]
then
    usage
    exit 1
fi

FICHERO="$1"
IP=$(/sbin/ip -o -4 addr list wlp2s0 | awk '{print $4}' | cut -d/ -f1)

EXTENSION="${FICHERO##*.}"

echo
echo "Poncio, con tu paspanismo habitual tienes que hacer esto:"
echo "1. Abre vlc en el ordenador de mamá,"
echo "2. Vete al menú Medio->Abrir ubicación en red"
echo "3. Añade la siguiente dirección: $IP:8080"
echo "4. Haz click en 'Reproducir'"
echo

if [[ $EXTENSION == "mp4" ]]
then
    #vlc "${FICHERO}" --sout '#std{access=http,mux=ffmpeg{mux=flv},dst=:8080/}'
    OPCIONES="--sout "#std{access=http,mux=ffmpeg{mux=flv},dst=:8080/}""
elif [[ $EXTENSION == "avi" ]]
then
    OPCIONES="--sout http/ts://:8080"    
elif [[ $EXTENSION == "mkv" ]]
then
    OPCIONES="--sout http/ts://:8080"
    echo
    echo "Atención, paspancio: los ficheros mkv no parece que funcionen bien en la streaming! "
    echo
else
    echo "Lo siento, paspancio. Formato desconocido para emitir."
    exit 1
fi

vlc "${FICHERO}" ${OPCIONES}
