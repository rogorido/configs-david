#!/bin/bash

ROJO='\e[1;31m'
AZUL='\e[1;34m'
NORMAL='\e[0m'

# --relative sirve para que reproduzca la estructura de archivos
#OPCIONESGENERALES='--relative --progress'
OPCIONESGENERALES='--progress'

DIRECTORIOS=("/home/david/Escritorio/general" "/home/david/Escritorio/documentos/traduccion"
	     "/home/david/Escritorio/documentos/Turk"
	     "/home/david/.mozilla/firefox/mwad0hks.default/zotero" "/home/david/.mozilla/firefox/mwad0hks.default/*.sqlite"
	     "/home/david/.local/share/mnemosyne"  "/home/david/Escritorio/imaginacion" )

# en el caso del pincel solo pasamos unos elementos concretos
REDUCIDO=0

rsyncear () {
    if [ ${REDUCIDO} -eq 1 ]
    then
	# pasamos solo unos directorios determinados
	for i in 0 3 4 5
	do
    	    rsync -avz $OPCIONESGENERALES $OPCIONES ${DIRECTORIOS[$i]} "${DESTINO}"
	done
    else
	for i in "${DIRECTORIOS[@]}"
	do
    	    rsync -avz $OPCIONESGENERALES $OPCIONES  $i "${DESTINO}"
	done
    fi
}

PS3='Escoge el destino de la copia: '
options=("Casa" "Casa (externamente)" "Disco duro" "pincel" "Salir")
select opt in "${options[@]}"
do
    case $opt in
        "Casa")
	    DESTINO='david@192.168.1.101:/home/david/copiaseguridad'
	    break;;
        "Casa (externamente)")
	    DESTINO='david@oviedocomputadora.chickenkiller.com:/home/david/copiaseguridad'
	    break;;
        "Disco duro")
	    DESTINO='/media/david/HP Portable Drive/copiaseguridad'
	    break;;
        "pincel")
	    DESTINO='/media/david/TOSHIBA/copiaseguridad'
	    REDUCIDO=1
	    break;;
        "Salir")
            exit
            ;;
        *) echo Opción inválida;;
    esac
done

echo
echo 

PS3='Oh, gran paspán, ¿Borramos en el destino lo borrado en el origen? '
options=("Sí" "No" "Salir")
select opt in "${options[@]}"
do
    case $opt in
        "Sí")
	    OPCIONES='--delete'
	    break;;
        "No")
	    OPCIONES=''
	    break;;
        "Salir")
            exit
            ;;
        *) echo Opción inválida;;
    esac
done

rsyncear
